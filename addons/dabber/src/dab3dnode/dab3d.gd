@tool
class_name dab3D
extends Node3D

## dab3D Node
##
## A node that lets you dab (scatter) meshes onto surfaces or volumes.
## [br]
## Add a [dab3DResource] resource in order to control the scattering.

# MIT License
# Copyright (c) 2023 Donn Ingle (donn.ingle@gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


# TODO MAYBE: Use only a multimesh and offer the metamultimesh settings in this node instead.
# TODO I want to split the various concerns in this file out into the resource
# classes like daDab etc. This file it too long.
# TODO Is there some way to NOT have this script be on the dab3DNode at all?
# Like perhaps move all the exports into an actual control panel in the main
# view window or something? I don't like that the node cannot have some other
# script on it in an easy way.


enum PROBE_TYPE {UNKNOWN, DAB_TARGET, ATTRACTOR, REPELLER}
enum WARN {PARENT_SCALE, DAB3D_RESOURCE_RESET} #, FS_ERROR}

const _VERBOSE := false

# some shortcuts for looooong var names
const UTILS = NSdabber.dabUtils
const ROTS = NSdabber.dabSettingsRotation
const SCALES = NSdabber.dabSettingsScale
const MASKS = NSdabber.dabSettingsMasking
const D3DRES = NSdabber.dab3DResource
const DAB = NSdabber.dabDab


# ---------------- Export vars and inspector stuff -------------- #


## Add other dab3D nodes to this array and when you dab this node, all
## of them will dab. (If they do not have their own Dab3d Resources,
## they will also use the one this node uses.[br]
## IOW, the linked-dabbers will all dab with the same resource. This
## is useful for scattering many places with the same items all at once.
var linked_dabbers : Array[NSdabber.dab3D]
#var linked_dabbers : Array[NodePath]


## The list of the "Dab" [dabDab] resources which
## will be used by this Node.
var dab_3d_resource : D3DRES:
	set(dl):
		if dl == dab_3d_resource : return
		dab_3d_resource = dl
		notify_property_list_changed() #go update the related props


## A seed for the random number generator.
var seed := 0:
	get: return seed
	set(value):
		seed = value


## Multiply the number of instances to generate.
var count_multiplier := 1:
	get: return count_multiplier
	set(value):
		count_multiplier = value


## A multiplier for the visibility range.
## Bigger means you will see this from further away.
var visibility_range_multiplier : float = 1.0:
	get: return visibility_range_multiplier
	set(n):
		visibility_range_multiplier = n
		update_vis_range_in_all_multimesh_nodes()


## The collision layer that the rays should seek while instancing
var ray_collision_layer : int = 1


## Offers a choice of other types of MultimeshInstance3D nodes.
## [br]
## If you have the MetaMultimesh3D plugin enabled, then it will
## be available as an option.
var multimesh_node_type = 0


## Directory where you want to save multimesh resource files.
## [br]
## If [b]empty or null[/b] then the resources will be saved inside
## your scene file as usual.[br]
## (Beware, this can bloat your scene file.)
var multimesh_save_as: String


## Defines the cursor shape.
var cursor: int = 0: # Think of this as "cursor index"
	get:
		return cursor
	set(value):
		cursor = value
		direction = 0
		notify_property_list_changed()
		update_gizmos()


var _my_aco:NSdabber.CursorBase # my current active cursor object
var _current_cursor_index:int = 0 # my current cursor index into cursors_classnames_array
var active_cursor_object:NSdabber.CursorBase:
	get:
		if not NSdabber.cursor_dict_class_to_paf:
			push_error("Missing `cursor_dict_class_to_paf` in dabber plugin.gd")
			return null
		if _my_aco and (cursor == _current_cursor_index):
			return _my_aco # No change
		# There is a call for a new cursor, so make ONE instance
		var clsname = NSdabber.cursors_classnames_array[cursor]
		var clspaf = NSdabber.cursor_dict_class_to_paf[clsname]
		var MyClass:Script = load(clspaf)
		_my_aco = null # try to clear ref count from prev use?
		_my_aco = MyClass.new()
		assert(_my_aco.get_script() == MyClass, "Cursor class %s failed to load" % clspaf)
		_my_aco.target = target
		_my_aco.direction = direction
		_my_aco.cursor_size = placement_size
		_current_cursor_index = cursor
		return _my_aco


## The placement size of the bounding box. You can use the
## gizmos to change this too.
var placement_size := Vector3(10.0, 10.0, 10.0):
	get: return placement_size
	set(value):
		placement_size = value.clamp(Vector3.ONE * 0.01, Vector3.ONE * 5000.0)
		if not active_cursor_object: return
		active_cursor_object.cursor_size = placement_size


## The direction of the scatter.
var direction:int:
	set(i):
		direction = i
		if not active_cursor_object: return
		active_cursor_object.direction = i
		update_gizmos()


## The target of the scatter.
var target:int:
	set(i):
		target = i
		if not active_cursor_object: return
		active_cursor_object.target = i
		update_gizmos()


## A function that controls the pattern of the scatter.
var function:int

var show_or_hide_dab_cursor:bool = false # button and normal var

## Fill a volume or dab onto a surface.
var volume:bool = false


## Dynamic Interface stuff happens in here:
func _get_property_list():
	var ret = []
	if Engine.is_editor_hint():
		# Version of these vars for when Dabber plugin is enabled and working.
		var USE:int = PROPERTY_USAGE_DEFAULT
		var ARRAY_USE:int = PROPERTY_USAGE_EDITOR | PROPERTY_USAGE_STORAGE | PROPERTY_USAGE_SCRIPT_VARIABLE

		# When the dabber plugin is disabled (or even has a syntax error and won't 'compile),
		# IOW - it's reverted to a normal Node3D, I want the properties to be invisible, BUT
		# STILL be saved - so that what the user had when the the plugin was working/enabled
		# will be recovered between reloads.
		# The mechanism for this is the dabber_is_enabled boolean along with PROPERTY_USAGE_*
		# consts Godot provides.
		var dabber_is_enabled := true
		if not EditorInterface.is_plugin_enabled("dabber"):
			dabber_is_enabled = false
			USE = PROPERTY_USAGE_NO_EDITOR # This one is store-only AND invisible
			# This is for when it's an Array (e.g. linked_dabbers)
			ARRAY_USE = PROPERTY_USAGE_NO_EDITOR | PROPERTY_USAGE_SCRIPT_VARIABLE # trial and error man...

		# Moved this to the top so the Multimesh save dir is
		# more in your face.
		ret.append({
			"name": &"Multimesh",
			"type": TYPE_NIL,
			"usage": PROPERTY_USAGE_GROUP,
			"hint_string" : &"multimesh_"
		 })
		var items = ["MultiMeshInstance3D"]
		ret.append({
			"name": &"multimesh_save_as",
			"type": TYPE_STRING,
			"hint": PROPERTY_HINT_DIR,
			"usage" : USE
		 })
		# Is there any other multimesh thing out there?
		# TODO : make this more thorough...

		#print("MM??:", EditorInterface.is_plugin_enabled("MetaMultimeshInstance3D"))
		#print("RW??:", EditorInterface.is_plugin_enabled("resource_wrangler"))

		if does_class_exist("MetaMultimeshInstance3D"):
			items.append("MetaMultimeshInstance3D")
		# Make the list-box
		ret.append({
			"name": &"multimesh_node_type",
			"type": TYPE_INT,
			"usage": USE,
			"hint": PROPERTY_HINT_ENUM,
			"hint_string": ",".join(items)
		})

		ret.append({
			"name": &"Cursor",
			"type": TYPE_NIL,
			"usage": PROPERTY_USAGE_GROUP,
		 })
		ret.append({
			 "name": &"cursor",
			 "type": TYPE_INT,
			 "usage": USE,
			 "hint": PROPERTY_HINT_ENUM,
			 "hint_string": NSdabber.cursors
					if dabber_is_enabled else ""
		 })
		ret.append({
			 "name": &"direction",
			 "type": TYPE_INT,
			 "usage": USE,
			 "hint": PROPERTY_HINT_ENUM,
			 "hint_string": active_cursor_object.directions
					if dabber_is_enabled else ""
		 })
		ret.append({
			 "name": &"target",
			 "type": TYPE_INT,
			 "usage": USE,
			 "hint": PROPERTY_HINT_ENUM,
			 "hint_string": active_cursor_object.targets
					if dabber_is_enabled else ""
		 })
		ret.append({
			 "name": &"function",
			 "type": TYPE_INT,
			 "usage": USE,
			 "hint": PROPERTY_HINT_ENUM,
			 "hint_string": active_cursor_object.functions
					if dabber_is_enabled else ""
		 })
		ret.append({
			"name": &"show_or_hide_dab_cursor",
			"type": TYPE_BOOL,
			"usage": USE,
			"hint_string": "button:Show/Hide Cursor:basic"
					if dabber_is_enabled else ""
		})
		ret.append({
			"name":"volume",
			"type": TYPE_BOOL,
			"usage":USE
		})
		ret.append({
			"name": &"placement_size",
			"type": TYPE_VECTOR3,
			"usage": USE,
		 })
		ret.append({
			"name": &"ray_collision_layer",
			"type": TYPE_INT,
			"usage": USE,
			"hint": PROPERTY_HINT_LAYERS_3D_PHYSICS
		 })

		ret.append({
			"name": &"Dabs",
			"type": TYPE_NIL,
			"usage": PROPERTY_USAGE_GROUP,
		 })
		ret.append({
			"name": &"dab_3d_resource",
			"type": TYPE_OBJECT,
			"usage": USE,
			"hint": PROPERTY_HINT_RESOURCE_TYPE
		 })
		ret.append({
			"name": &"seed",
			"type": TYPE_INT,
			"usage": USE,
			"hint_string": "1,1000",
			"hint": PROPERTY_HINT_RANGE
		 })
		ret.append({
			"name": &"count_multiplier",
			"type": TYPE_INT,
			"usage": USE,
			"hint_string": "1,100,or_greater",
			"hint": PROPERTY_HINT_RANGE
		 })
		#ret.append({
			#"name": &"avoid_overlap",
			#"type": TYPE_BOOL,
			#"usage": USE
		#})
		ret.append({
			"name": &"linked_dabbers",
			"type": TYPE_ARRAY,
			"hint": PROPERTY_HINT_TYPE_STRING,
			"usage": ARRAY_USE,
			"hint_string": "%d/%d:dab3D" % [TYPE_OBJECT, PROPERTY_HINT_NODE_TYPE]
		 })
		ret.append({
			"name": &"visibility_range_multiplier",
			"type": TYPE_FLOAT,
			"usage": USE,
		 })
		ret.append({
			"name": &"dab",
			"type": TYPE_BOOL,
			"usage": USE, #PROPERTY_USAGE_EDITOR,
			"hint_string": "button:Do the Dab:main"
					if dabber_is_enabled else ""
		})
		ret.append({
			"name": &"remove_dabs",
			"type": TYPE_BOOL,
			"usage": USE, #PROPERTY_USAGE_EDITOR,
			"hint_string": "button:Remove dabs:basic"
					if dabber_is_enabled else ""
		})
	return ret


# --------------------------- Public Vars ----------------------------- #


var shapesize:
	get:
		return placement_size
	set(s):
		if s:
			placement_size = s

var hitboxes:Node3D


# --------------------------- Private Vars ----------------------------- #


var _dabber_has_valid_attractor := false
var _missing_resources:bool
var _pool : Dictionary
var _rng := RandomNumberGenerator.new()
var _s8 := "*".repeat(8)
var _settings:Dictionary
var _warnings := {}

@onready var _space_state: PhysicsDirectSpaceState3D = get_world_3d().direct_space_state


# --------------------------- Overrides ----------------------------- #


func _ready():
	if Engine.is_editor_hint():
		set_notify_transform(true)
		ensure_multimesh_resource_path()
		warn_on_parent_scaling()


func _get_aabb()->AABB:
	return AABB(position, placement_size)


func _get_configuration_warnings():
	for w in _warnings.values():
		push_warning(w)
	return _warnings.values()


func _notification(what: int) -> void:
	if !is_inside_tree(): return

	# A way to print notifications without them repeating.
	#if what != _last_notify_int:
	#	print("notify:", what)
	#	_last_notify_int = what

	# Deny Scale on me or parents
	if what == NOTIFICATION_TRANSFORM_CHANGED:
		if scale != Vector3.ONE:
			set_ignore_transform_notification(true)
			scale = Vector3.ONE # cos this ALTERS the transform!
			set_ignore_transform_notification(false)

		warn_on_parent_scaling()


## Button's signal sent from the inspectorbutton plugin
func _on_button_pressed(text:String):
	if Engine.is_editor_hint():
		match text.to_lower():
			&"dab":
				rescatter_if_in_editor()
			&"show_or_hide_dab_cursor":
				toggle_cursor_visiblity()
			&"remove_dabs":
				rm_unlocked_mmesh_children()

		#Do this last
		notify_property_list_changed()


# ------------------------ The main Scatter Func ------------------------- #


# The main scatter func
# This is a long and tricksy func. Be careful in there!
#region MAIN SCATTER FUNC
func scatter() -> void:
	var time_start := Time.get_ticks_msec()
	const verbose := _VERBOSE
	if not multimesh_save_as:
		if not ensure_multimesh_resource_path(): return

	prepare_defaults()

	# To get _rng repeating the same pattern each time
	_rng.seed = seed

	# Before we begin, we remove all unlocked multimesh nodes
	# NOTE: This ALSO rms the saved multimesh files (if any)!
	rm_unlocked_mmesh_children()

	# A node (invisible) to hold hitboxes if overlap test requested
	mk_hitboxes_node()

	## TODO
	## If avoid_overlap is true, we have to seek possible aabb
	## intersections of *other* dab3D nodes.
	## If any, then must make hitboxes for all the transforms in all
	## the multimeshes under that node.
	## IDEA: Only those mmtransforms *within* my (this dab3Dnode) AABB
	## need be visited.
	#if _settings[MASKS].avoid_overlap:
		#var list = await detect_other_dab3d_nodes()
		#if list:
			#pass

	## Mix Texture and related vars
	#var _dablist_size:float
	#var _mix_span:float
	#var mix_dabs_texture:Texture
	#var mix_image:Image
	#if dab_3d_resource.mix_dabs_texture:
		## TODO use the tex_to_img funcs instead
		#mix_dabs_texture = dab_3d_resource.mix_dabs_texture
		#if mix_dabs_texture is Texture2D:
			#mix_image = mix_dabs_texture.get_image()
			#_dablist_size = dab_3d_resource.dabs.size()
			#_mix_span = 1.0/_dablist_size

	# Walk through the dabs in my list
	for dab in dab_3d_resource.dabs:
		#region Settings and Setup
		# Make a settings dict of what's plugged-in
		# Fetch default instances from the pool to cover blanks
		_settings.clear()

		for _s in [MASKS, ROTS, SCALES]:
			_settings[_s] = _pool[_s] # get new blank/default objects

		# Now, if we have specific settings, use those instead:
		if dab.dab_settings_masking:
			_settings[MASKS] = dab.dab_settings_masking
		if dab.dab_settings_rotation:
			_settings[ROTS] = dab.dab_settings_rotation
		if dab.dab_settings_scale:
			_settings[SCALES] = dab.dab_settings_scale

		if not dab.mesh_sources:
			dab.mesh_sources = _pool[DAB].mesh_sources

		if verbose:
			print(_s8,"rays",_s8)
			print("cursor: %s" % cursor)
			print("direction: %s" % direction)

		# Position Texture
		var position_image:Image = _settings[MASKS].get_image_or_null(&"texture")

		if verbose: print(_s8,"Dab's Meshes",_s8)
		#endregion Settings and Setup

		# May 2024: extract the meshes for re-use
		var the_dabs_meshes:Array[Mesh] = dab.get_cached_meshes()

		var mesh_index:int = 0

		# Walk through all the meshes
		for a_mesh in the_dabs_meshes:
			mesh_index += 1
			var transforms = []

			if not a_mesh:
				feedback("dabMesh %s has no mesh." % dab)
				continue

			if verbose: print(" mesh :%s" % a_mesh)

			# Make MultiMeshNode name.
			# I want it to be unique across reloads, hence I
			# cannot use object strings like str(self) etc.
			# It must also have something that ties-it to dab3d node.
			var resname:String = a_mesh.get(&"resource_name") # or null implied
			if not resname:
				resname = a_mesh.get_class()
			var dn:String = dab.name if dab.name else "unnamedDab"
			var np_hash := str(NodePath(self.get_path()).hash())
			var multimesh_node_name:String = "%s_%s_%s" % [resname, dn, np_hash]

			if verbose:
				print("np_hash:", np_hash)
				print("dn:", dn)
				print("Multimesh node name:", multimesh_node_name)

			# If it's still there after the last scatter, it's
			# locked— *because* we skipped it last time.
			var existing_mmeshnode_locked = find_child(multimesh_node_name)

			if existing_mmeshnode_locked:
				# loop the multimesh transforms and build the overlap hitboxes
				# for the next dab to scatter into (possibly).
				var mmesh : MultiMesh = existing_mmeshnode_locked.multimesh
				for ti in mmesh.instance_count:
					var tform = mmesh.get_instance_transform(ti)
					overlapping(tform, dab, mmesh.mesh)
				# just in case the visibility settings have changed
				setup_visibility_range(existing_mmeshnode_locked)
			else:
				# DO THE SCATTER
				# Note, unlocked multimesh3d nodes have been skipped.

				var probes:Array[RayProbe]

				# Get all possible hits (probes) - filter by position_image too
				probes = get_possible_probes(dab, a_mesh,
						the_dabs_meshes.size(), mesh_index, position_image)

				# Also remove masked-out ones.
				var valid_probes:Array = filter_probes_by_group_masking(probes)

				probes.clear() # don't need this anymore

				# Alright, we have a list of valid probes. Make transforms,
				# mutate and store them.
				#region -- Process the valid probes --
				for v_probe:RayProbe in valid_probes:
					var finalhit_dict := v_probe.target_dict
					var new_basis : Basis
					if volume:
						# NOTE: INTENDED SIDE-EFFECT: Changes `finalhit_dict`
						new_basis = volume_basis(finalhit_dict, v_probe, position_image)
					else:
						# SURFACE SCATTER - works with global coords.
						new_basis = surface_basis(dab, v_probe, verbose)

					# Our shiny new transform!
					var t := Transform3D(new_basis)

					#region SCALE ROT
					# -- Mess with rotation --#
					if _settings[ROTS]:
						t = _settings[ROTS].mess_with(t, v_probe, placement_size, _rng)

					# -- Mess with the scale --#
					if _settings[SCALES]:
						t = _settings[SCALES].mess_with(t, v_probe, placement_size, _rng)

					# NOTE: NB do this AFTER the above rot and scale!
					if volume:
						# volume is working in local coords
						# November 2023.
						# top_level had a bug in 4.2b
						# Feb 2024: It's fixed.
						#t.origin = t.origin - position #?
						t.origin = finalhit_dict.position
					else:
						# November 2023: top_level had a bug in 4.2b
						# Feb 2024: They Fixed the set_top_level bug
						# Thus using top_level on multimesh again:
						t.origin = finalhit_dict.position
						# surface is in global coords thus this doozy
						# which I can't grok any more :
						t = get_global_transform().affine_inverse() * t
						# but don't do this...
						# t.origin = t.origin - position
						# ?
						# This was part of the work-around for top_level bug.
						# It's kind of buggy now...
						# *CONFIRMED* This calcs a LOCAL SPACE within the
						# dab3d node.
						# I read this as "take the global_position out of
						# finalhit_dict.position"
						# This goes with set_top_level on the individual
						# multimeshes.
						#t.origin = finalhit_dict.position - global_position

					#endregion SCALE ROT

					#region Overlap check

					# If dab is overlapping, skip it.
					# Calculated from the AABB.
					if _settings[MASKS].avoid_overlapping:
						if await overlapping(t, dab, a_mesh):
							continue

					transforms.append(t)

					#endregion Overlap

				#endregion -- Process Valid probes --

				if verbose: print("transforms size: %s" % transforms.size())

				# Now we can multimeshify the transforms:
				#region Multimeshify
				if transforms.size() > 0:
					# Make the MultiMesh
					var multimesh : MultiMesh
					var mmnode : MultiMeshInstance3D

					# Is there a preferred mmNode3d type?
					match multimesh_node_type:
						0:
							mmnode = MultiMeshInstance3D.new()
						1:
							# This is another of my addons
							mmnode = new_custom_node("MetaMultimeshInstance3D")

					assert(mmnode, "MultimeshInstance3D.new() failed.")

					# Copied from HungryProton Scatter:
					# https://github.com/HungryProton/scatter/blob/6821b63a120f4c003da7e4d6f9ec28658e8337dc/addons/proton_scatter/src/common/scatter_util.gd#L124
					# "if set_name is used after add_child it is crazy slow
					# This doesn't make much sense but it is definitely the case.
					# About a 100x slowdown was observed in this case"
					mmnode.set_name(multimesh_node_name)
					setup_visibility_range(mmnode)

					self.add_child(mmnode,true)

					mmnode.set_owner(self.owner)
					multimesh = MultiMesh.new()

					# This is the only way I could get the math to work...
					# As of Nov 2023 (Godot 4.2b) there's a bug with top_level
					# I managed to find a work-around in the math above
					# but I don't know when this will bite me...
					#mmnode.position = self.position
					mmnode.top_level = true # top_level bug has been fixed

					# If there's a save path, then use it.
					# NOTE: Save them as binary "res" files because tres files
					# are immense.
					if multimesh_save_as:
						var paf = multimesh_save_as + "/" + multimesh_node_name
						paf = paf + ".res"
						multimesh.take_over_path(paf)

					multimesh.instance_count = 0
					multimesh.transform_format = MultiMesh.TRANSFORM_3D
					multimesh.mesh = a_mesh
					mmnode.multimesh = multimesh

					var icount:int = 0
					var has_custom_data : bool = (dab.custom_data
						and dab.custom_data.uses_data(a_mesh.resource_path))

					var has_custom_color : bool = (dab.custom_data
						and dab.custom_data.uses_color(a_mesh.resource_path))

					multimesh.set_use_custom_data(has_custom_data)
					multimesh.set_use_colors(has_custom_color)

					multimesh.instance_count = transforms.size() #reset

					#var aabb:AABB = multimesh.mesh.get_aabb()
					for t in transforms:
						# if there's custom data, put it in
						if has_custom_data:
							multimesh.set_instance_custom_data(
									icount, dab.custom_data.get_data(
										a_mesh.resource_path, t, icount))
						if has_custom_color:
							multimesh.set_instance_color(
									icount, dab.custom_data.get_color(
										a_mesh.resource_path, t, icount))
						# plonk the transform in
						multimesh.set_instance_transform(icount, t)
						icount += 1

					# If user wants the multimesh resource saved, do so:
					if multimesh_save_as:
						# TODO handle errors?
						var err = ResourceSaver.save(multimesh)

				#endregion Multimeshify
		# end for a_mesh loop

	# NOTE: This entire set of loops finishes MUCH faster than
	# the multimeshes may actually take to appear. Godot kind of
	# freezes and I don't know how to deal with that.
	# This mostly happens on large scatters with lots of stuff.
	var time_end := Time.get_ticks_msec()
	var diff := ( time_end - time_start )
	var sec := diff / 1000.0
	print_rich("[b]Dabber[/b] took [color=yellow]%f[/color] seconds (%000.00fms)" % [sec,diff] )
	rm_hitboxes_node()
#endregion MAIN SCATTER FUNC


# ------------------------ Public Funcs ------------------------- #
# -- These are also called from plugin.gd

func my_gizmo_hide():
	var giz := get_my_gizmo_or_null()
	if giz:
		giz.set_hidden(true)


func my_gizmo_show():
	var giz := get_my_gizmo_or_null()
	if giz:
		giz.set_hidden(false)


# ------------------------ Private Funcs ------------------------- #


func feedback(txt):
	push_warning(txt)


# Warn if parent nodes have any non ONE scale
func warn_on_parent_scaling():
	var p := get_parent_node_3d()
	# if we are scale ONE again, reset warning
	if p and p.scale == Vector3.ONE:
		_warnings.erase(WARN.PARENT_SCALE)
		update_configuration_warnings()
	else:
		_warnings[WARN.PARENT_SCALE] = "Parent node (%s) is scaled. This will cause reality distortion effects." % p.name
		update_configuration_warnings()


## If the user has a string in multimesh_save_as, then
## that is used to make a folder for future saving into.[br]
## If multimesh_save_as is empty, nothing happens. (i.e.
## the multimesh resources will be saved inside the scene file)
func ensure_multimesh_resource_path()->bool:
	if multimesh_save_as.is_empty():
		return true # user does not want to save files

	if not DirAccess.dir_exists_absolute(multimesh_save_as):
		#There no directory, so make one
		var err = DirAccess.make_dir_recursive_absolute(multimesh_save_as)
		if err != OK:
			## This is some dire shit:
			if _VERBOSE: push_error("Please check your filesystem: error ",
			err, " at path: ",multimesh_save_as)
			feedback("Please check your filesystem: error %s at %s" %
					[err,multimesh_save_as])
			return false
	return true


func get_my_gizmo_or_null()->EditorNode3DGizmo:
	var g = get_gizmos()
	if g:
		return g.back()
	return null


func toggle_cursor_visiblity():
	show_or_hide_dab_cursor = !show_or_hide_dab_cursor
	var giz := get_my_gizmo_or_null()
	if giz:
		giz.set_hidden(false) # gizmo may have been hidden
	update_gizmos()


# NOTE: Also deletes resource files!
func rm_unlocked_mmesh_children():
 	# catch MultiMeshInstance3D too
	var existing_nodes := find_children("*","MultiMeshInstance3D")
	for n in existing_nodes:
		if not n.get_meta("_edit_lock_", false):
			# If there's a multimesh resource file we must rm it too
			if n.multimesh:
				var paf = n.multimesh.resource_path
				if FileAccess.file_exists(paf):
					var err = DirAccess.remove_absolute(paf)
					if err != OK:
						if _VERBOSE:push_error("Cannot remove file:", paf)
						feedback("Cannot remove file: %s" % paf)
			remove_child(n)
			n.multimesh = null
			n.queue_free()


func does_class_exist(cname):
	var a := ProjectSettings.get_global_class_list()
	a = a.filter( func(i): return i.class == cname )
	return not a.is_empty()


# To be able to load another plugin like the MetaMultimesh3DNode
# Without using its class_name explicitly in code - so that if
# it's not installed it won't break this plugin.
func new_custom_node(cname):
	var a := ProjectSettings.get_global_class_list()
	a = a.filter( func(i): return i.class == cname )
	#print(a)
	#[{ "class": &"MetaMultimeshInstance3D", "language": &"GDScript", "path": "res://addons/MetaMultimeshInstance3D/src/MetaMultimeshInstance3D.gd", "base": &"MultiMeshInstance3D", "icon": "" }]
	if a:
		var _new = load(a[0].path).new()
		return _new
	return null


func do_i_replace_dab3dresource()->bool:
	var _tmp = _pool.get(D3DRES, null)
	if _tmp == null:
		return true
	return dab_3d_resource == _tmp


# This prepares the _pool dict.
func prepare_defaults():
	UTILS.dict_set_from_get_or_new(_pool,MASKS)
	UTILS.dict_set_from_get_or_new(_pool,ROTS)
	UTILS.dict_set_from_get_or_new(_pool,SCALES)

	UTILS.dict_set_from_get_or_new(_pool, D3DRES)
	UTILS.dict_set_from_get_or_new(_pool, DAB)
	_pool[DAB].count = 10
	_pool[DAB].mesh_sources.clear()
	_pool[DAB].mesh_sources.append(NSdabber.DEFAULT_SPHERE)

	if not dab_3d_resource:
		# Stick an empty dab into the list if there are none.
		dab_3d_resource = _pool[D3DRES]
	else:
		# Sometimes the export remembers a resource that is bad
		# or not cached
		var reset:bool = false
		var msg:=""
		if dab_3d_resource.resource_path.is_empty():
			dab_3d_resource = _pool[D3DRES]
			msg = "resource_path is empty"
			reset = true
		# not cached, so probably bad in some way.
		if not ResourceLoader.has_cached(dab_3d_resource.resource_path):
			dab_3d_resource = _pool[D3DRES]
			msg = "cache failed mysteriously"
			reset = true
		_warnings.erase(WARN.DAB3D_RESOURCE_RESET)
		if reset:
			_warnings[WARN.DAB3D_RESOURCE_RESET] = \
				"The dab3DResource for this Node has been reset to default because the %s." % msg
		update_configuration_warnings()

	if not dab_3d_resource.dabs:
		# Stick an empty dab into the dabs list if there are none.
		dab_3d_resource.dabs.append(_pool[DAB])


## Gets all the possible probes (RayProbe class).
func get_possible_probes(dab:DAB, a_mesh:Mesh, num_meshes:int,
		mesh_index:int, position_image:Image) -> Array[RayProbe]:
	var probe:RayProbe
	var probes:Array[RayProbe]
	var fromtos:Array[NSdabber.CursorBase.FromTo]
	var palgo:Callable = active_cursor_object.get_function(function)
	var count:int = dab.count * count_multiplier

	# Makes all <count> fromto objs in one place.
	fromtos = palgo.call({
		rng = _rng,
		count = count,
		mesh_index = mesh_index,
		num_meshes = num_meshes,
		mesh_aabb = a_mesh.get_aabb(),
	})
	_dabber_has_valid_attractor = false

	for ft in fromtos:
		# Now work out the start and end of a ray from rndpos
		# The positions are calculated in LOCAL SPACE
		# of the placement shape
		var rfrom:Vector3 = ft.from
		var rto:Vector3 = ft.to

		# Make a probe object
		probe = RayProbe.new(rto, rfrom)

		# TODO, maybe:Have each dab "grow" spherically to sense obstacles
		# around it so that things scale to the gaps they are in
		# and don't clip into colliders around them
		# (like a box above them)

		# The ray requires from and to to be global coords
		# TODO : this might fail if the dab3d nodes are children
		# of some other node...
		# NOTE: A 'Probe' is a series of hits along the ray.
		# We are not going to detect back-faces (because brain too small)
		# This means only the frontal-direction of all colliders in the
		# path of the ray will be detected.
		# ray start -> hit a thing -> hit a different thing -> fin
		# NOT
		# ray start -> hit a thing -> inside the thing -> leaving the thing -> .. fin
		rfrom =  transform * rfrom
		rto = transform * rto
		var ray_query := PhysicsRayQueryParameters3D.create(
			rfrom, rto, ray_collision_layer)

		# Build a list of 'hits' along the length of the ray.
		# (Does this by shooting the SAME ray over and over but
		# progressively noting and then ignoring (via set_exclude)
		# each thing it has already hit.)
		# Also tests for attractor and repeller groups.
		# This info is all in the hits inside the rayprobe.
		var _excludes=[]
		while true:
			var probe_type = PROBE_TYPE.UNKNOWN
			var ahit_dict = _space_state.intersect_ray(ray_query)

			# I bleev this is the final state at ray's end
			if ahit_dict.is_empty():
				break

			# Remember to ignore whatever we hit this time, next time round
			_excludes.append(ahit_dict.rid)
			ray_query.set_exclude(_excludes)

			# Can only hit visible things
			if ahit_dict.collider.visible:
				# Decide what kind of collider we are dealing with.
				# A hit with a group is probably a repel/attract
				# i.e. not a mesh to be scattered onto, but to guide.
				# If it isn't, then it's just target mesh.
				probe_type = PROBE_TYPE.UNKNOWN

				for grp in ahit_dict.collider.get_groups():

					if grp in _settings[MASKS].get_repel_groups():
						probe_type = PROBE_TYPE.REPELLER
						break # exit GROUP loop

					if grp in _settings[MASKS].get_attract_groups():
						probe_type = PROBE_TYPE.ATTRACTOR
						_dabber_has_valid_attractor = true
						break # exit GROUP loop

				# After all that group looping, if we are still unknown:
				if probe_type == PROBE_TYPE.UNKNOWN:
					probe_type = PROBE_TYPE.DAB_TARGET # then we ARE the target

				# It's possible that all we hit is attractors all the way along
				probe.push_layer(ahit_dict, probe_type)

				# Another early-out of the WHILE loop
				if (probe_type == PROBE_TYPE.DAB_TARGET):
					break

			# End of if collider visible test

		# End of while loop - we have some kind of probe

		# Is the rto (local space) valid per the noise map?
		# Avoid any place that is cutoff.
		if (probe.has_target and position_image):
			rto = to_local(probe.target_dict.position)
			if UTILS.get_luminance(position_image, rto, placement_size) >\
				_settings[MASKS].texture_cutoff:
				continue

		# Because of VOLUME scatters, weird probes are valid.
		# E.g. empty, or all attractors. So just add the probe.
		# Will sort that out in filter_probes_by_group_masking()
		probes.append(probe)

	# End of fromtos loop
	return probes


# This is a loop over all probes, NOT down each probe's length!
func filter_probes_by_group_masking(probes:Array[RayProbe]) -> Array[RayProbe]:
	var valid_probes:Array[RayProbe]
	for a_probe:RayProbe in probes:
		if volume: # Volume scatter
			# volumes start from ray_from and go to ray_to
			# (if there's anything* in the way just stop there
			# by setting the ray_to to the pos of the hit.)
			# All probes, even empty ones, are valid for this
			# volume scatter.
			# * Anything means Attractors or Repellers or Any colliders
			var _hit := a_probe.get_first_hit()
			if _hit:
				a_probe.local_ray_to = to_local(_hit.position)

			valid_probes.append(a_probe)

		else: # Surface scatter
			if a_probe.invalid_for_surface_scatter():
				continue

			if a_probe.repeller_first:
				continue

			# Wow, this logic is boggly! But it works!
			# This is the mechanism that excludes 'default' placements
			# when there is an Attractor, which must get all the attention.
			if _dabber_has_valid_attractor:
				if a_probe.has_target and a_probe.attractor_first:
					valid_probes.append(a_probe)
			else:
				valid_probes.append(a_probe)

	return valid_probes


# We just take a random length along vector from->to and plonk
# an instance there. When there's ANY collider inside a volume,
# it is avoided to make a hole.
# NOTE WTF? this only works with local coords. Shrugs.
func volume_basis(finalhit_dict, v_probe, position_image) -> Basis:
	var rfrom : Vector3 = v_probe.local_ray_from
	var rto:Vector3 = v_probe.local_ray_to
	var _rnd := randf()
	if position_image:
		_rnd = UTILS.get_luminance(position_image, rto, placement_size)
	#finalhit_dict["position"] = rfrom.lerp(rto, randf())
	finalhit_dict["position"] = rfrom.slerp(rto, _rnd)
	return Basis(finalhit_dict.position.normalized(),0)


# This is where orientation and wobble is used.
func surface_basis(dab, v_probe, verbose) -> Basis:
	# 100% sure - v_probe.target_dict.position is GLOBAL SPACE

	# HOLY FKCNSHZ!! I did a thing with my lame-brain and it worked!
	# Works out where the ray hit and makes a Basis, that actually
	# looks good, for the dabbed-mesh.
	var vprobe_normal:Vector3 = v_probe.target_dict.normal
	#var new_X:Vector3 = finalhit_dict.normal.cross(
	var new_X:Vector3 = vprobe_normal.cross(
		global_transform.basis.z)
	# If the normal and the basis.z are parallel
	# volunteer a diff axis.
	# <---vprobe_normal--- X --basis.z---> == ZERO
	# ----vprobe_normal--> X <---basis.z-- == ZERO
	if new_X.is_zero_approx():
		new_X = global_transform.basis.x

	var new_Y:Vector3 =  vprobe_normal#finalhit_dict.normal
	var new_Z := new_X.cross(new_Y)#vprobe_normal)#finalhit_dict.normal)

	# try to wobble the vector a bit
	var _wobble:Vector3

	#if _settings[ROTS].orient_wobble > 0:
	if dab.orient_wobble > 0:
		var r := randf_range(0,dab.orient_wobble)
		var ang:= randf_range(0,TAU)
		_wobble = Vector3(
			cos(ang)*r,
			0,
			sin(ang)*r
			)
	new_Y = new_Y + _wobble

	# Basis only deals with rotation and scale. NOT position
	var new_basis:Basis = Basis(
			new_X,
			new_Y,
			new_Z,
		).orthonormalized()

	if dab.orient_to_vector:
		# TODO: maybe limit this to a gravity thing?
		# up or down only. forget left right etc.
		var orient_vec:Vector3 = dab.orient_vector

		var new_orient:Vector3 = orient_vec
		if dab.orient_wobble > 0:
			new_orient = _wobble + orient_vec

		#https://maththebeautiful.com/dot-product/
		#A dot Normal > 0 if A in same general dome as vprobe_normal
		#             < 0 if pointing opp.
		#var _dot : float = finalhit_dict.normal.dot(new_orient)
		var _dot : float = vprobe_normal.dot(new_orient)
		if verbose:
			print(_s8)
			print("new_orient:", new_orient)
			print(" new_Z:", new_Z)
			print(" dot:", _dot)
		if _dot > 0:
			var is_parallel := new_Z.cross(new_orient) == Vector3.ZERO
			if not is_parallel:
				new_basis= Basis.looking_at(new_Z, new_orient)
	#print("new_basis:", new_basis)
	return new_basis


func rm_hitboxes_node():
	if hitboxes and not hitboxes.is_queued_for_deletion():
		remove_child(hitboxes)
		hitboxes.queue_free()


func mk_hitboxes_node():
	hitboxes = find_child("hitboxes")
	rm_hitboxes_node()
	hitboxes = Node3D.new()
	hitboxes.name = "hitboxes"
	add_child(hitboxes)
	#hitboxes.set_owner(owner) # don't have to do this
	await get_tree().process_frame # VERY NB


func mk_hitbox(aabb:AABB):
	var m:MeshInstance3D = MeshInstance3D.new()
	m.mesh = BoxMesh.new()
	m.name = "dabber__hitbox__%s" % randi()
	m.transparency = 0.0 # 0 means solid boxes
	m.position = aabb.get_center()
	m.mesh.size = aabb.size
	hitboxes.add_child(m)
	# v nb to get this shape into bvh properly
	await get_tree().process_frame


# Ret true means we are skipping the transform because
# it's overlapping. This makes use of Godot's built-in
# BVH volume system. It's a bit clunky, but better than
# anything I can make.
# NOTE: I tried making hitboxes with the server methods, but the
# BVH only works on VisualInstance NODES.
func overlapping(t:Transform3D, dab, a_mesh) -> bool:
	var local_aabb:AABB = a_mesh.get_aabb()
	local_aabb = local_aabb.grow(_settings[MASKS].margin)

	# get_aabb is in some mesh-local-space
	# it must be moved to t (inside dabnode3d space)
	# Then moved up to global.
	# (Also removed top_level in mk_hitbox)
	var global_aabb:AABB = global_transform * t * local_aabb

	# Not working in 4.4 dev
	# This one looks better. It might be the top_level thing...
	#var global_aabb:AABB = t * local_aabb

	# NB await BEFORE the cull_aabb. It won't work at all else.
	await get_tree().physics_frame
	# cull_aabb works in global space
	var pa:PackedInt64Array = RenderingServer.instances_cull_aabb(
			global_aabb, get_world_3d().scenario)
	# PackedInt64Array does not have filter method :(
	for objid in pa:
		var i = instance_from_id(objid)
		if "dabber__hitbox" in i.name:
			#print(i.name)
			# we have hit a pre-existing hitbox
			return true
	# ∴ we hit nothing, so place a box - but it's a child of
	# this dab3d node, so it needs to be in local space
	local_aabb = t * local_aabb
	mk_hitbox(local_aabb)
	return false

#TODO
#func detect_other_dab3d_nodes():
	#var myaabb #get from the cursor i guess?
	#pass


func update_vis_range_in_all_multimesh_nodes():
	var existing_nodes := find_children("*","MultiMeshInstance3D") #catches metamultimesh too
	for n in existing_nodes:
		if not n.get_meta("_edit_lock_", false):
			setup_visibility_range(n)


# Gives sane numbers to the vis range.
# TODO: fix magic numbers
func setup_visibility_range(mm:MultiMeshInstance3D):
	mm.visibility_range_begin = 0
	mm.visibility_range_begin_margin = 1
	mm.visibility_range_end = 11 * visibility_range_multiplier
	mm.visibility_range_end_margin = 1
	mm.visibility_range_fade_mode = GeometryInstance3D.VISIBILITY_RANGE_FADE_SELF


func rescatter_if_in_editor() -> void:
	if !_space_state:
		return
	if Engine.is_editor_hint():
		# Handle the linked-dabbers array!

		# Put myself into the list
		if not self in linked_dabbers:
			linked_dabbers.append(self)

		for dabnode in linked_dabbers:
			# If no dab list at all or has the default sphere
			if dabnode.do_i_replace_dab3dresource():
				# Replace with my dab3d resource
				dabnode.dab_3d_resource = dab_3d_resource
			dabnode.scatter()

		# Take myself out of the list— don't want that saved.
		var me := linked_dabbers.find(self)
		if me > -1:
			linked_dabbers.remove_at(me)


# --------------------------- Rayprobe Class ----------------------------- #


# Helper to manage ray "probes".
# I think of a probe as a core-sample like when they
# drill down into permafrost to see the layers.
class RayProbe:
	var target_dict:Dictionary

	var _stack:Array
	# _stack will eventually be one of:
	# 1. [Repeller, ...whatever] -> repeller_first
	# 2. [Attractor, ..same, Attractor] -> invalid
	# 3. [Attractor, ...whatever, Target] -> attractor_first
	# 4. [Target] -> has_target

	# Case 1
	var repeller_first:bool:
		get:
			return _stack.front().type == PROBE_TYPE.REPELLER
	# Case 3
	var attractor_first:bool:
		get:
			return _stack.front().type == PROBE_TYPE.ATTRACTOR
	# Case 4
	var has_target:bool

	# used in volume scatter
	func get_first_hit() -> Dictionary:
		if _stack.is_empty(): return {}
		return _stack.front().hitdict

	var local_ray_to:Vector3
	var local_ray_from:Vector3

	func _init(to,from):
		local_ray_to = to
		local_ray_from = from

	func invalid_for_surface_scatter() -> bool:
		# An empty stack or one with only Attractors is invalid
		# for the normal surface scatter mode.
		return (_stack.is_empty()
		or _stack.all(func(e): return e.type == PROBE_TYPE.ATTRACTOR) )

	func push_layer(_hitdict:Dictionary, _type) -> void:
		if _type == PROBE_TYPE.DAB_TARGET:
			has_target = true
			target_dict = _hitdict
		_stack.append({type =_type, hitdict = _hitdict})

	func dump():
		print("*****",self)
		print("  stack:")
		for layer in _stack:
			print("type: %s collider: %s" % [layer.type, layer.hitdict.collider])
		print()




























# CODE BONE ZONE




#func _get_image_from_texture_if_in_dict(_settings, _c)-> Image:
	#var tex:Texture
	#var img:Image
	#if _c in _settings:
		#tex = _settings[_c].get(&"texture") # or null
		#if tex is Texture2D:
			#img = tex.get_image()
			#return img
	#return null


#func _get_image_from_texture(tex_in:Texture2D)-> Image:
	#var tex:Texture
	#var img:Image
	#if tex_in is Texture2D:
		#img = tex_in.get_image()
		#return img
	#return null


## Old mix image idea. Not sure how to implement it
## If you use a mix texture, each 'dab' will be
## distributed across the texture by their index
## in the dabs array.
#@export var mix_dabs_texture : Texture2D
### How much to mingle the edges of the mix texture by.
#@export_range(0, 100.0, 1, "or_greater") var mix_mingle : float = 1.0
		## If there's a mix image, use it.
		#if mix_image:
			#var _noise = randf_range(0, (dab_3d_resource.mix_mingle/10000))
			#var _lum = UTILS.get_luminance(mix_image, rto, placement_size)
			#var _start_mix_range : float
			#var _end_mix_range : float
			#_start_mix_range -= _noise
			#_end_mix_range += _noise
			#if _lum <= _start_mix_range or _lum >= _end_mix_range:
				#continue


# NOTE : Tried make GeometryInstance3D directly and just set their AABB
# Does not work ☹ Thought it might be quicker.
#func mk_hitbox(aabb:AABB):
	#var g:GeometryInstance3D = GeometryInstance3D.new()
	#g.cast_shadow = GeometryInstance3D.SHADOW_CASTING_SETTING_OFF
	#g.ignore_occlusion_culling = true
	#g.gi_mode = GeometryInstance3D.GI_MODE_DISABLED
	#g.custom_aabb = aabb
	#g.name = "dabber__hitbox__%s" % randi()
	#g.position = aabb.get_center()
	#hitboxes.add_child(g)
	## v nb to get this shape into bvh properly
	#await get_tree().process_frame

#
#
#class XXRayProbe:
	#var is_attractor:bool
	#var attractor={}
	#var attractor_visible:bool:
		#get:
			#if not attractor: return false
			#return attractor.collider.visible
#
	#var is_repeller:bool
	#var repeller={}
	#var repeller_visible:bool:
		#get:
			#if not repeller: return false
			#return repeller.collider.visible
#
	#var on_target:bool
	#var target_dict:Dictionary
	#var _target_hits:Array
	#var hits:Array
#
	#var local_ray_to:Vector3
	#var local_ray_from:Vector3
#
	#func _init(to,from):
		#local_ray_to = to
		#local_ray_from = from
#
	#func push(_hitdict:Dictionary, _type) -> void:
		##print("PUSH into ", self)
		##print("type:", _type)
		##print("hitdict:", _hitdict)
		##print()
		#if _type == PROBE_TYPE.ATTRACTOR:
			#is_attractor = true
			#attractor = _hitdict # the last one detected
			##target_dict = _hitdict
#
		#if _type == PROBE_TYPE.REPELLER:
			#is_repeller = true # the last one detected
			#repeller = _hitdict
			##target_dict = _hitdict
#
		#if _type == PROBE_TYPE.DAB_TARGET:
			#if _hitdict.collider.visible:
				#_target_hits.append(_hitdict)
				#on_target = true
				#target_dict = _target_hits.front()
#
		#hits.append(_hitdict)
#
	#func already_has(hit):
		#return hit in hits
#
	#func is_empty():
		#return hits.is_empty()
#
	#func dump():
		#print("*****",self)
		#print("    is_attractor:bool:",is_attractor )
		#print("    attractor:",attractor )
		#print("    attractor_visible:bool:",attractor_visible )
		#print("    is_repeller:bool:",is_repeller )
		#print("    repeller:",repeller )
		#print("    repeller_visible:bool:",repeller_visible )
		#print("    on_target:bool:",on_target )
		#print("    target_dict:",target_dict )
		#print()


#func setup_debug_meshes():
#		# MORE DEBUG MESHES
#	var raay : Node3D = get_node_or_null("../rays")
#	if raay:
#		#for idx in raay.get_child_count():
#		#	raay.queue_free()
#		get_parent().remove_child(raay)
#		raay.propagate_call("queue_free")
#	raay = Node3D.new()
#	raay.name = "rays"
#	get_parent().add_child(raay)
#	raay.set_owner(self.owner)
#
#	var debugs = find_children("","MeshInstance3D")
#	if debugs:
#		for d in debugs:
#			remove_child(d)
#			d.queue_free()

#func draw_debug_meshes():
#	var dotm = SphereMesh.new()
#	dotm.radius = 0.01
#	dotm.height = 0.02
#	var FROM_node = MeshInstance3D.new()
#	FROM_node.mesh = dotm
#
#	raay.add_child(FROM_node)
#	FROM_node.set_owner(self.owner)
#	FROM_node.position = rfrom
#
#	var TO_node = MeshInstance3D.new()
#	TO_node.mesh = BoxMesh.new()
#	TO_node.mesh.size = Vector3(0.02,0.02,0.02)
#
#	raay.add_child(TO_node)
#	TO_node.set_owner(self.owner)
#	TO_node.position = rto


#var _last_parent_scale := Vector3.ONE
#func OLD_look_for_weird_parent_scaling():
	#var p := get_parent_node_3d()
	#print(p)
	## make sure we only do anything when the SCALE HAS CHANGED
	#if p and p.scale != _last_parent_scale:
		#_last_parent_scale = p.scale
		## if we are scale ONE again, reset warning
		#if p and p.scale == Vector3.ONE:
			#_warnings.erase(WARN.PARENT_SCALE)
			#update_configuration_warnings()
		#else:
		## if the scale is BAD, warn
			#_warnings[WARN.PARENT_SCALE] = "Parent node (%s) is scaled. This will cause reality distortion effects." % p.name
			#update_configuration_warnings()
