@tool
extends Button

signal quick_dab_signal

func _on_pressed() -> void:
	quick_dab_signal.emit()
