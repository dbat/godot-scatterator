@tool
class_name dabCursorSphere
extends dabCursorBase

# MIT License
# Modified Work Copyright (c) 2023-present Donn Ingle.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

enum TARGET {ORIGIN}
enum DIRECTION {PING, PONG, PINGPONG}

var _gizmo_mesh_cached:Dictionary
var gizmo_mesh:Dictionary:
	get: return _gizmo_mesh_cached

var _cursor_mesh := SphereMesh.new()
var cursor_mesh:Mesh:
	get:
		_cursor_mesh.radius = cursor_size.x/2.
		_cursor_mesh.height = cursor_size.x
		return _cursor_mesh

func _init() -> void:
	name = "Sphere"
	words = {
		# keep names in same order as enums
		&"target": [&"Origin"],
		# keep names in same order as enums
		&"direction": [&"Ping", &"Pong", &"Pingpong"],
		&"function": [
			{"Random":rnd_point_on_surface},
			{"Grid Stacked":grid_on_sphere},
			{"Grid Spread": diagonals_spread_on_sphere}
		]
	}
	_gizmo_mesh_cached = _get_gizmo_mesh()



var algos:Dictionary={}:
	get: return {"Random on surface": rnd_point_on_surface}

var alternate:bool


func rnd_point_on_surface(_args) -> Array[FromTo]:
	var rng = _args["rng"]
	var count = _args["count"]
	var fromtos:Array[FromTo]
	var radius : float = cursor_size.x * 0.5

	for i in range(0,count):
		var x = rng.randf_range(-1,1)
		var y = rng.randf_range(-1,1)
		var z = rng.randf_range(-1,1)
		var v3 = Vector3(x,y,z).normalized() * radius
		fromtos.append(_new_from_to(radius, v3))
	return fromtos


func grid_on_sphere(_args) -> Array[FromTo]:
	var fromtos:Array[FromTo]
	var s:float; var t:float; var x:float; var y:float; var z:float
	var r := cursor_size.x/2.0
	var step:float
	#var n : int = _args["num_meshes"]
	step = 360 / sqrt(_args["count"])
	# n is kind of the space needed for this mesh
	#n += (_args["mesh_aabb"] as AABB).get_longest_axis_size()

	# drift is to shuffle through space to fit multiple meshes
	#var drift : int = (360 / n) * _args["mesh_index"]

	for ds in range(0, 360, step):
		for dt in range(0, 360, step):
			s = deg_to_rad(ds)# + deg_to_rad(drift)
			t = deg_to_rad(dt)# + deg_to_rad(drift)
			x = r * cos(s) * sin(t)
			y = r * sin(s) * sin(t)
			z = r * cos(t)
			var v3 = Vector3(x,z,y) # yeah yeah
			fromtos.append(_new_from_to(r, v3))
	return fromtos


func diagonals_spread_on_sphere(_args) -> Array[FromTo]:
	var fromtos:Array[FromTo]
	var s:float; var t:float; var x:float; var y:float; var z:float
	var r := cursor_size.x/2.0
	var step:float
	var n : int = _args["num_meshes"]
	step = 360 / sqrt(_args["count"])
	# n is kind of the space needed for this mesh
	n += (_args["mesh_aabb"] as AABB).get_longest_axis_size()

	# drift is to shuffle through space to fit multiple meshes
	var drift : int = (360 / n) * _args["mesh_index"]

	for ds in range(0, 360, step):
		for dt in range(0, 360, step):
			s = deg_to_rad(ds) + deg_to_rad(drift)
			t = deg_to_rad(dt) + deg_to_rad(drift)
			x = r * cos(s) * sin(t)
			y = r * sin(s) * sin(t)
			z = r * cos(t)
			var v3 = Vector3(x,z,y) # yeah yeah
			fromtos.append(_new_from_to(r, v3))
	return fromtos
#
#
#func hacks(_args) -> Array[FromTo]:
	#var fromtos:Array[FromTo]
	#var s:float; var t:float; var x:float; var y:float; var z:float
	#var r := cursor_size.x/2.0
	#var step:float
	#var n : int = _args["num_meshes"]
	#step = 360 / sqrt(_args["count"])
	#print("step:", step)
	#print("n:", n)
	#var drift : int = (360 / n) * _args["mesh_index"]
	#print("drift:", drift)
	##return []
#
	##step = (129600.0 / int(_args["count"])) # 360^2 = 129600
	##step = 8 if step == 0 else step
	#for ds in range(0, 360, step):
		##print("ds:", ds)
		#for dt in range(0, 360, step):
			##print("dt:", dt)
			#s = deg_to_rad(ds) + deg_to_rad(drift)
			#t = deg_to_rad(dt) + deg_to_rad(drift)
			#x = r * cos(s) * sin(t)
			#y = r * sin(s) * sin(t)
			#z = r * cos(t)
			#var v3 = Vector3(x,z,y) # yeah yeah
			#fromtos.append(_new_from_to(r, v3))
	#return fromtos


#func grid_on_sphere(_args) -> Array[FromTo]:
	#var fromtos:Array[FromTo]
	#var s:float; var t:float; var x:float; var y:float; var z:float
	#var r := cursor_size.x/2.0
	#var step:float
	#step = (129600.0 / int(_args["count"])) # 360^2 = 129600
	#step = 8 if step == 0 else step
	#for ds in range(0, 360, step):
		#for dt in range(0, 360, step):
			#s = deg_to_rad(ds)
			#t = deg_to_rad(dt)
			#x = r * cos(s) * sin(t)
			#y = r * sin(s) * sin(t)
			#z = r * cos(t)
			#var v3 = Vector3(x,z,y) # yeah yeah
			#fromtos.append(_new_from_to(r, v3))
	#return fromtos


func _new_from_to(height, rnd_point):
	var from:Vector3 = rnd_point
	var to:Vector3 = Vector3.ZERO
	# direction swaps from/to
	match direction:
		DIRECTION.PONG:
			var tmp = from; from = to; to = tmp
		DIRECTION.PINGPONG:
			if alternate:
				var tmp = from; from = to; to = tmp
			alternate = !alternate
	return FromTo.new(from, to)






## ------------------------------GIZMOS----------------------------------##




## The basic gizmo that shows where this dab3D is
## It can be selected by the mouse.
func _get_gizmo_mesh() -> Dictionary:
	var mesh = SphereMesh.new()
	mesh.radius = 0.5
	mesh.height = mesh.radius * 2.0
	mesh.radial_segments = 8
	mesh.rings = 8
	var tris = mesh.generate_triangle_mesh()
	#mesh = make_wire_complex(mesh)
	var d := {tris=tris, mesh=mesh}
	return d


## The arrows to illustrate the Direction and Target
## of this Sphere cursor.
func get_arrows() -> Array[Arrow]:
	var arr:Array[Arrow]
	var A : Arrow
	var radius : float = cursor_size.y / 2.5 #magic number!
	var from : Vector3 = Vector3(0, radius, 0)
	var to : Vector3 = (from - Vector3.ZERO)/2.0
	if direction == DIRECTION.PONG:
		var tmp = from
		from = to
		to = tmp
	for deg in range(0,360,(360/10)):
		A = Arrow._new(from, to, direction == DIRECTION.PINGPONG)
		var a = deg_to_rad(deg)
		A.t = A.t.rotated(Vector3.LEFT, a)
		arr.append(A)

	return arr


## What happens when gizmo handles are dragged
func handle_drag_to_change_cursor_size(size, _handle_id) -> Vector3:
	var a:float
	# All three handles affect the radius
	match _handle_id:
		0: a = size.x
		1: a = size.y
		2: a = size.z
	return Vector3(a,a,a)
