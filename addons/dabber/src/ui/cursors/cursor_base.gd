class_name dabCursorBase
extends RefCounted

# MIT License
# Modified Work Copyright (c) 2023-present Donn Ingle.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# this is a cursor object
# sets a shape of some kind
# can draw that as a gizmo
# can draw the direction vector gizmo (one or more)
# provides the algos for each point
# change algo - changes available dirs, targets and redaws gizmo

# not intended as an RW node
# dab3d picks each of these cursor classes up (somehow)
# and lets you choose among them.


var name:String = "unset"
var cursor_size:=Vector3(10,10,10)
var direction:int
var target:int
var words := {}


var directions:String:
	get:
		if not words: return ""
		return ",".join(words.direction)

var targets:String:
	get:
		if not words: return ""
		return ",".join(words.target)

var functions:String:
	get:
		if not words: return ""
		var a = words.function.map(func(e):return e.keys()[0])
		return ",".join(a)

func get_function(i:int) -> Callable:
	var fncs = words.function.map(func(e):return e.values()[0])
	return fncs[i]


func _get_gizmo_mesh() -> Dictionary: return {}


## Scans all the classes and looks for anything that extends
## this file.
static func get_cursor_class_paths() -> Dictionary:
	var a := ProjectSettings.get_global_class_list()
	#print(str(a).replace("},","\n"))
	var b = a.filter(func(i):return i.base == &"dabCursorBase")
	# b is like [{ "class": &"dabCursorBox", "language": &"GDScript",
	# "path": "res://addons/dabber/src/gizmos/cursor_box.gd",
	# "base": &"dabCursorBase", "icon": "" },..]
	var dict:={}
	#if b:
	for c in b:
		dict[c.class] = c.path
	return dict


# override me
func get_arrows(): pass# -> Array[Arrow]: return []

# Abstract
static func get_from_to(cursor, direction, target, rng, placement_size, count, index) -> FromTo: return null

class FromTo:
	var from:Vector3
	var to:Vector3
	static var alternate:bool = false
	func _init(_f:Vector3,_t:Vector3):
		from = _f
		to = _t
	#func rotate(axis, angle):
		#from = from.rotated(axis,angle)
		#to = to.rotated(axis,angle)
	func _dump():
		return "from:%s, to:%s" % [from, to]


## A helper to create the 3D gizmo arrows
class Arrow:
	const radius := 0.2
	const res := 8
	const VERT := ArrayMesh.ARRAY_VERTEX
	const NORM := ArrayMesh.ARRAY_NORMAL
	var mesh : Mesh
	var t : Transform3D

	func _init(_mesh) -> void:
		mesh = _mesh

	static func _new(from:Vector3, to:Vector3, two_way:=false) -> Arrow:
		var length = (from-to).length()
		var tip2 : CylinderMesh
		var tip := CylinderMesh.new()

		#The pointy tip(s)
		tip.radial_segments = res
		tip.rings = 1
		tip.top_radius = radius
		tip.bottom_radius = 0
		tip.height = radius * 2
		tip.cap_top = false
		if two_way:
			tip2 = tip.duplicate()

		# Move tip down and rotate it
		var tip_shift = tip.get_mesh_arrays()
		for i in range(0,tip_shift[0].size()):
			tip_shift[VERT][i]\
				 -= Vector3(0, length - tip.height/2.0, 0)
			tip_shift[VERT][i]\
				 = tip_shift[VERT][i].rotated(Vector3.RIGHT, PI/2.0)
			tip_shift[NORM][i]\
				 = tip_shift[NORM][i].rotated(Vector3.RIGHT, PI/2.0)

		var tip_shift2
		if two_way:
			# Move the second tip up
			tip_shift2 = tip2.get_mesh_arrays()
			for i in range(0,tip_shift2[0].size()):
				tip_shift2[VERT][i]\
					 -= Vector3(0, tip.height/2.0, 0)
				tip_shift2[VERT][i]\
					 = tip_shift2[VERT][i].rotated(Vector3.RIGHT, -PI/2.0)
				tip_shift2[NORM][i]\
					 = tip_shift2[NORM][i].rotated(Vector3.RIGHT, -PI/2.0)

		#The stem
		var stem := CylinderMesh.new()
		var smidge := tip.height
		length -= smidge
		stem.radial_segments = res
		stem.rings = 1
		stem.bottom_radius = radius
		stem.top_radius = radius
		stem.cap_bottom = false
		if two_way: stem.cap_top = false
		stem.height = length
		# Move stem up a bit and lie it along the Z pointing into screen
		var stem_shift = stem.get_mesh_arrays()
		for i in range(0,stem_shift[0].size()):
			stem_shift[VERT][i] -= Vector3(0,length/2.0,0)
			stem_shift[VERT][i] = stem_shift[VERT][i].rotated(Vector3.RIGHT, PI/2.0)
			stem_shift[NORM][i] = stem_shift[NORM][i].rotated(Vector3.RIGHT, PI/2.0)

		# Glue those two into one thing
		var arr_mesh = ArrayMesh.new()
		arr_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, stem_shift)
		arr_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, tip_shift)
		if two_way:
			arr_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, tip_shift2)

		var A := Arrow.new(arr_mesh)
		# Now deploy looking_at to point it at 'to'
		A.t = Transform3D()
		A.t.origin = from
		var upish:Vector3 = (A.t.basis.y + A.t.basis.z).normalized()
		A.t = A.t.looking_at(to, upish)
		return A


## What happens when gizmo handles are dragged
## TODO put this into the Cylinder cursor when it gets done.
func handle_drag_to_change_cursor_size(size, _handle_id) -> Vector3:
	# Figure out what to do with the 'size' var
	#var cursorEnum = NSdabber.dabRays.CursorEnum
	#match dab3d_node.cursor:
		#cursorEnum.CYLINDER:
			#var a = size.x
			#var b = size.y
			#var c = size.z
			#match _handle_id: #who has JUST CHANGED?
				#AXIS.Z: a = c
			#dab3d_node.shapesize = Vector3(a,b,a)

	return size # box is easy
