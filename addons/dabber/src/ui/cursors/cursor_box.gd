class_name dabCursorBox
extends dabCursorBase

# MIT License
# Modified Work Copyright (c) 2023-present Donn Ingle.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

enum TARGET {TOP_ACROSS, ALL_ACROSS}
enum DIRECTION {PING, PONG, PINGPONG}

var alternate:bool

var _gizmo_mesh_cached:Dictionary
var gizmo_mesh:Dictionary:
	get: return _gizmo_mesh_cached


var _cursor_mesh := BoxMesh.new()
var cursor_mesh:Mesh:
	get:
		_cursor_mesh.size = cursor_size
		#print("get cursor_mesh at size:", cursor_size)
		return _cursor_mesh


func _init() -> void:
	name = "Box"
	words = {
		# keep target names in same order as target enums
		&"target": [&"One Across", &"All Across"],
		# keep dir names in same order as dir enums
		&"direction": [&"Ping", &"Pong", &"Pingpong"],
		&"function": [{"Random": random_funcs}, {"Grid": grid_funcs}],
	}
	_gizmo_mesh_cached = _get_gizmo_mesh()


func random_funcs(_args:Dictionary) -> Array[FromTo]:
	match target:
		TARGET.TOP_ACROSS: return rnd_points_on_top_face(_args)
		TARGET.ALL_ACROSS: return rnd_points_on_all_faces(_args)
	return []


func grid_funcs(_args:Dictionary) -> Array[FromTo]:
	match target:
		TARGET.TOP_ACROSS: return grid_points_on_top_face(_args)
		TARGET.ALL_ACROSS: return grid_points_on_all_faces(_args)
	return []


## makes rnd points on all faces
func rnd_points_on_all_faces(_args:Dictionary) -> Array[FromTo]:
	var rng = _args["rng"]
	var count = _args["count"]
	var cpf := int(count)
	var fromtos:Array[FromTo]
	var rnd_point:Vector3
	for facevec in [Vector3.UP, Vector3.DOWN, Vector3.LEFT, Vector3.FORWARD, Vector3.RIGHT, Vector3.BACK]:
		for i in range(0, cpf):
			match facevec:
				Vector3.UP:
					rnd_point = Vector3(
						rng.randf_range(-cursor_size.x / 2.0, cursor_size.x / 2.0),
						cursor_size.x / 2.0,
						rng.randf_range(-cursor_size.z / 2.0, cursor_size.z / 2.0)
					)
				Vector3.DOWN:
					rnd_point = Vector3(
						rng.randf_range(-cursor_size.x / 2.0, cursor_size.x / 2.0),
						-cursor_size.x / 2.0,
						rng.randf_range(-cursor_size.z / 2.0, cursor_size.z / 2.0)
					)
				Vector3.FORWARD:
					rnd_point = Vector3(
						rng.randf_range(-cursor_size.x / 2.0, cursor_size.x / 2.0),
						rng.randf_range(-cursor_size.y / 2.0, cursor_size.y / 2.0),
						-cursor_size.z / 2.0,
					)
				Vector3.BACK:
					rnd_point = Vector3(
						rng.randf_range(-cursor_size.x / 2.0, cursor_size.x / 2.0),
						rng.randf_range(-cursor_size.y / 2.0, cursor_size.y / 2.0),
						cursor_size.z / 2.0,
					)
				Vector3.LEFT:
					rnd_point = Vector3(
						-cursor_size.x / 2.0,
						rng.randf_range(-cursor_size.y / 2.0, cursor_size.y / 2.0),
						rng.randf_range(-cursor_size.z / 2.0, cursor_size.z / 2.0)
					)
				Vector3.RIGHT:
					rnd_point = Vector3(
						cursor_size.x / 2.0,
						rng.randf_range(-cursor_size.y / 2.0, cursor_size.y / 2.0),
						rng.randf_range(-cursor_size.z / 2.0, cursor_size.z / 2.0)
					)
			fromtos.append(_new_from_to(rnd_point, facevec))
	return fromtos

## makes rnd points on top face
func rnd_points_on_top_face(_args:Dictionary) -> Array[FromTo]:
	var fromtos:Array[FromTo]
	var rng = _args["rng"]
	var count = _args["count"]
	for i in range(0,count):
		var rnd_point = Vector3(
			rng.randf_range(-cursor_size.x / 2.0, cursor_size.x / 2.0),
			cursor_size.y / 2.0,
			rng.randf_range(-cursor_size.z / 2.0, cursor_size.z / 2.0)
		)
		fromtos.append(_new_from_to(rnd_point))
	return fromtos


## Make grid on the top face only
## The count is only a vague density with this one.
func grid_points_on_top_face(_args:Dictionary) -> Array[FromTo]:
	return _grid_on_face(_args, Vector3.UP)


## Make grids on all the faces
func grid_points_on_all_faces(_args:Dictionary) -> Array[FromTo]:
	var ret:Array[FromTo]
	for facevec in [Vector3.LEFT, Vector3.RIGHT, Vector3.UP,
		Vector3.DOWN, Vector3.FORWARD, Vector3.BACK]:
		ret.append_array(_grid_on_face(_args, facevec))
	return ret


# A generic make grid of points on a face func. Using a crude sqrt approach
func _grid_on_face(_args:Dictionary, facevec) -> Array[FromTo]:
	var colsz:float; var rowsz:float
	# what plane are the columns and rows in (and their sizes)?
	match facevec:
		Vector3.LEFT, Vector3.RIGHT:
			colsz = cursor_size.z; rowsz = cursor_size.y
		Vector3.FORWARD, Vector3.BACK:
			colsz = cursor_size.x; rowsz = cursor_size.y
		Vector3.UP, Vector3.DOWN:
			colsz = cursor_size.x; rowsz = cursor_size.z
	var fromtos:Array[FromTo]
	var count:int = _args["count"]
	var sq := ceili(sqrt(count))
	var size_col:float = colsz / sq
	var size_row:float = rowsz / sq
	var ps:Array
	for column in range(0, sq):
		for row in range(0, sq):
			var p:Vector3
			# what plane do we make the points on?
			match facevec:
				Vector3.DOWN, Vector3.UP:
					p = Vector3(column * size_col, 0, row * size_row)
				Vector3.FORWARD, Vector3.BACK:
					p = Vector3(column * size_col, row * size_row, 0)
				Vector3.RIGHT, Vector3.LEFT:
					p = Vector3(0, row * size_row, column * size_row)
			ps.append(p)
	# have to loop again to get it all centered properly
	# because I am a dumb. First get the min maxes per face
	var max_across:float; var max_high:float
	match facevec:
		Vector3.UP, Vector3.DOWN:
			max_across = ps.max().x; max_high = ps.max().z
		Vector3.FORWARD, Vector3.BACK:
			max_across = ps.max().x; max_high = ps.max().y
		Vector3.LEFT, Vector3.RIGHT:
			max_across = ps.max().z; max_high = ps.max().y
	# Now to adjust each face by the actual max and min of the points
	# so that they are centered in the cursor faces.
	for p in ps:
		match facevec:
			Vector3.UP:
				p = p - Vector3(max_across/2.0, -cursor_size.y / 2.0, max_high/2.0)
			Vector3.DOWN:
				p = p - Vector3(max_across/2.0, cursor_size.y / 2.0, max_high/2.0)
			Vector3.FORWARD:
				p = p - Vector3(max_across/2.0,  max_high/2.0, cursor_size.z / 2.0)
			Vector3.BACK:
				p = p - Vector3(max_across/2.0, max_high/2.0, -cursor_size.z / 2.0)
			Vector3.LEFT:
				p = p - Vector3(cursor_size.x, max_high/2.0, max_across/2.0)
			Vector3.RIGHT:
				p = p - Vector3(-cursor_size.x, max_high/2.0, max_across/2.0)
		# These are what become fromtos
		fromtos.append(_new_from_to(p, facevec))
	return fromtos


# Makes the fromto object that controls whether we are ping/pong or pingpong
func _new_from_to(rnd_point, facevec:=Vector3.UP) -> FromTo:
	var height:float = cursor_size.y
	var from:Vector3 = rnd_point
	var to:Vector3

	# target changes the to
	match target:
		TARGET.TOP_ACROSS:
			to = Vector3(from.x, from.y - height, from.z)
		TARGET.ALL_ACROSS:
			match facevec:
				Vector3.LEFT, Vector3.RIGHT:
					to = Vector3(0, from.y, from.z)
				Vector3.FORWARD, Vector3.BACK:
					to = Vector3(from.x, from.y, 0)
				Vector3.UP, Vector3.DOWN:
					to = Vector3(from.x, 0, from.z)
	# direction swaps from/to
	match direction:
		DIRECTION.PONG:
			var tmp = from; from = to; to = tmp
		DIRECTION.PINGPONG:
			if alternate:
				var tmp = from; from = to; to = tmp
			alternate = !alternate

	var ft := FromTo.new(from, to)
	return ft




## -------------------------Gizmos----------------------------------##




## The basic gizmo that shows where this dab3D is
## It can be selected by the mouse.
func _get_gizmo_mesh() -> Dictionary:
	var mesh = BoxMesh.new()
	mesh.size = Vector3(0.5,0.5,0.5)
	var tris = mesh.generate_triangle_mesh()
	#mesh = make_wire_complex(mesh)
	var d := {tris=tris, mesh=mesh}
	return d



## The arrows to illustrate the Direction and Target
## of this Box cursor. It's a bit wild.
func get_arrows() -> Array[Arrow]:
	#print("get_arrows")
	#print("target:", target)
	var all : PackedVector3Array
	var arr:Array[Arrow]
	var A : Arrow
	var from : Vector3
	var to : Vector3
	match target:
		TARGET.TOP_ACROSS:
			var top := Vector3(0,cursor_size.y/2.0,0)
			var bot := Vector3(0,-cursor_size.y/2.0,0)
			var x = 0
			var _d = direction
			match _d:
				DIRECTION.PING:
					from = top
					from.x = x
					to = bot
					to.x = x
					A = Arrow._new(from, to)
					arr.append(A)
				DIRECTION.PONG:
					from = bot
					from.x = x
					to = top
					to.x = x
					A = Arrow._new(from, to)
					arr.append(A)
				DIRECTION.PINGPONG:
					from = bot
					from.x = x
					to = top
					to.x = x
					A = Arrow._new(from, to, true)
					arr.append(A)

		TARGET.ALL_ACROSS:
			var y = 0
			var hh := cursor_size.y / 2.0
			var hw := cursor_size.x / 2.0
			var hd := cursor_size.z / 2.0
			for facevec in [Vector3.LEFT, Vector3.FORWARD, Vector3.RIGHT, Vector3.BACK, Vector3.UP, Vector3.DOWN]:
				match facevec:
					Vector3.LEFT:
						from = Vector3(-hw, y, 0)
						to = from - Vector3(-hw / 2.0, y, 0)
					Vector3.FORWARD:
						from = Vector3(0, y, -hd)
						to = from - Vector3(0, y, -hd / 2.0)
					Vector3.RIGHT:
						from = Vector3(hw, y, 0)
						to = from - Vector3(hw / 2.0, y, 0)
					Vector3.BACK:
						from = Vector3(0, y, hd)
						to = from - Vector3(0, y, hd / 2.0)
					Vector3.UP:
						from = Vector3(0, hh, 0)
						to = from - Vector3(0, hh / 2.0, 0)
					Vector3.DOWN:
						from = Vector3(0, -hh ,0)
						to = from - Vector3(0, -hh / 2.0, 0)

				# Keep for in case. It swaps dirs per arrow
				#var _d = direction
				#if direction == DIRECTION.PINGPONG:
					#alternate = !alternate
					#_d = DIRECTION.PONG
					#if alternate: _d = DIRECTION.PING

				if direction == DIRECTION.PONG:
					var tmp = from
					from = to
					to = tmp

				A = Arrow._new(from, to, direction == DIRECTION.PINGPONG)
				arr.append(A)

	return arr

## What happens when gizmo handles are dragged
func handle_drag_to_change_cursor_size(size, _handle_id) -> Vector3:
	return size # box is easy
