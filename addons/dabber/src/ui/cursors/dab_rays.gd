#@tool
#class_name dabRays
#extends RefCounted
#
### ------------- Public ------------- ##
#
#enum CursorEnum {FOR_BOX_CURSOR, FOR_CYLINDER_CURSOR, FOR_SPHERE_CURSOR}
#
### This one is not used right now
#static func get_directions(for_cursor:int):
	#return _mk_associations(for_cursor, false)
#
### Get a CSV of all the directions for a cursor int
#static func get_directions_csv(for_cursor:int):
	#return ",".join(_mk_associations(for_cursor, true))
#
### Get all the rays (callable funcs) for the cursor and direction
#static func get_rays(for_cursor:int, direction:int)->Array:
	#return _get_direction_callables(for_cursor, direction)
#
### A csv of the ray functions
#static func get_rays_csv(rays)->String:
	#return ",".join(rays.map(func(e): return e.get_method()))
#
#
### ------------- Private ------------- ##
#
#static var _cursorint_to_cursorclass = {}
#
### Assoc cursors with directions, used in two places
#static func _associate(for_cursor, direction)->Dictionary:
	#var clsref
	#var dtext:String
#
	#_cursorint_to_cursorclass[CursorEnum.FOR_BOX_CURSOR] = BoxCursor
	#_cursorint_to_cursorclass[CursorEnum.FOR_CYLINDER_CURSOR] = CylinderCursor
	#_cursorint_to_cursorclass[CursorEnum.FOR_SPHERE_CURSOR] = SphereCursor
#
	#var Dir = _cursorint_to_cursorclass[for_cursor].Dir
#
	#match for_cursor:
		#CursorEnum.FOR_BOX_CURSOR:
			#match direction:
				#Dir.UP: clsref = BoxCursor.UP; dtext="Up"
				#Dir.DOWN: clsref = BoxCursor.DOWN; dtext="Down"
				#Dir.UPDOWN: clsref = BoxCursor.UPDOWN; dtext="Up-down"
		#CursorEnum.FOR_CYLINDER_CURSOR:
			#match direction:
				#Dir.UP: clsref = CylinderCursor.UP; dtext="Up"
				#Dir.DOWN: clsref = CylinderCursor.DOWN; dtext="Down"
				#Dir.UPDOWN: clsref = CylinderCursor.UPDOWN; dtext="Up-down"
		#CursorEnum.FOR_SPHERE_CURSOR:
			#match direction:
				#Dir.IN: clsref = SphereCursor.IN;dtext = "Inwards"
				#Dir.OUT: clsref = SphereCursor.OUT;dtext = "Outwards"
				#Dir.INOUT: clsref = SphereCursor.INOUT;dtext = "In-Out"
#
	#return {clsref=clsref, dtext=dtext}
#
#static func get_cursorobj(for_cursor:int):
	#return _cursorint_to_cursorclass[for_cursor]
#
#static func _mk_associations(c, debug_flag:=false):
	#var ret = []
	#var cursorObj = _cursorint_to_cursorclass[c]
	#var Dir = cursorObj.Dir
	#for dir in Dir: #dir here is a string!
		#var a = _associate(c,Dir[dir]) #hence lookup key
		#if a.clsref:
			#if debug_flag:
				## might want the user-friendly text
				#ret.append(a.dtext)
			#else:
				## or just something for debugging
				##enum[i] gives the int
				#ret.append(Dir[dir])
	#return ret
#
#
#static func _filter(arr)->Array:
	#return arr.filter(
		#func(e):
			#var test = (e.id == 0 and e.flags==METHOD_FLAG_NORMAL | METHOD_FLAG_STATIC)
			#test = test and e.name != "free"
			#return test
			#)
#
#
#static func _get_direction_callables(for_cursor, direction)->Array:
	#var what = _associate(for_cursor, direction)
	#if what.clsref:
		#var methods = what.clsref.new().get_method_list()
		#var l = _filter(methods)
		#l = l.map(func(e): return Callable(what.clsref,e.name))
		#return l
	#return []
#
#
#class Ray:
	#var from:Vector3
	#var to:Vector3
	#func _init(_f:Vector3,_t:Vector3):
		#from = _f
		#to = _t
	#static func _new(_ray:Dictionary, _from:Dictionary)->Ray:
		#var from = Vector3(_from.x, _ray.from, _from.z)
		#var to = Vector3(  _from.x, _ray.to,   _from.z)
		#return Ray.new(from,to)
	#static func _new_by_from_to(_from:Vector3,_to:Vector3)->Ray:
		#var from = _from
		#var to = _to
		#return Ray.new(from,to)
#
#
#class BoxCursor:
	#enum Dir {DOWN, UP, UPDOWN}
	#class UP:
		#static func random_rectangle(data)->Ray:
			#var from = dabRays.rnd_rect(data)
			#var ray = dabRays.ray_up(data)
			#return Ray._new(ray, from)
#
		#static func regular_diamonds(data)->Ray:
			#var from = dabRays.reg_diamondish(data)
			#var ray = dabRays.ray_up(data)
			#return Ray._new(ray, from)
#
		#static func regular_diagonals(data)->Ray:
			#var from = dabRays.reg_diamondish(data)
			#var ray = dabRays.ray_up(data)
			#return Ray._new(ray, from)
#
	#class DOWN:
		#static func random_rectangle(data)->Ray:
			#var from = dabRays.rnd_rect(data)
			#var ray = dabRays.ray_down(data)
			#return Ray._new(ray, from)
#
		#static func regular_diamonds(data)->Ray:
			#var from = dabRays.reg_diamondish(data)
			#var ray = dabRays.ray_down(data)
			#return Ray._new(ray, from)
#
		#static func regular_diagonals(data)->Ray:
			#var from = dabRays.reg_diamondish(data)
			#var ray = dabRays.ray_down(data)
			#return Ray._new(ray, from)
#
	#class UPDOWN:
		#static func random_rectangle(data)->Ray:
			#var from = dabRays.rnd_rect(data)
			#var ray = dabRays.ray_up_down(data, from)
			#return Ray._new(ray, from)
#
		#static func regular_diamonds(data)->Ray:
			#var from = dabRays.reg_diamondish(data)
			#var ray = dabRays.ray_up_down(data, from)
			#return Ray._new(ray, from)
#
		#static func regular_diagonals(data)->Ray:
			#var from = dabRays.reg_diamondish(data)
			#var ray = dabRays.ray_up_down(data, from)
			#return Ray._new(ray, from)
#
#class CylinderCursor:
	#enum Dir {DOWN, UP, UPDOWN}
	#class UP:
		#static func random_disc(data)->Ray:
			#var from = dabRays.rnd_disc(data)
			#var ray = dabRays.ray_up(data)
			#return Ray._new(ray, from)
#
	#class DOWN:
		#static func random_disc(data)->Ray:
			#var from = dabRays.rnd_disc(data)
			#var ray = dabRays.ray_down(data)
			#return Ray._new(ray, from)
#
	#class UPDOWN:
		#static func random_disc(data)->Ray:
			#var from = dabRays.rnd_disc(data)
			#var ray = dabRays.ray_up_down(data, from)
			#return Ray._new(ray, from)
#
#
#class SphereCursor:
	#enum Dir {IN, OUT, INOUT}
	#class IN:
		#static func random_surface(data)->Ray:
			#var from:= dabRays.rnd_surface(data)
			#return Ray._new_by_from_to(from, Vector3.ZERO)
#
	#class OUT:
		#static func random_surface(data)->Ray:
			#var to:= dabRays.rnd_surface(data)
			#return Ray._new_by_from_to(Vector3.ZERO, to)
#
	#class INOUT:
		#static func random_surface(data)->Ray:
			#var rnd_surface:= dabRays.rnd_surface(data)
			#var ray = dabRays.ray_in_out(data, rnd_surface)
			#return Ray._new_by_from_to(ray.from,ray.to)
#
#
## ----------- Ray Point Funcs ----------- #
#static func rnd_rect(data:Dictionary)->Dictionary:
	#var rng = data.rng
	#var sz:Vector3 = data.placement_size
	#return {
		#x = rng.randf_range(-sz.x / 2.0, sz.x / 2.0),
		#z = rng.randf_range(-sz.z / 2.0, sz.z / 2.0) }
#
#
#static func rnd_disc(data:Dictionary)->Dictionary:
	#var rng = data.rng
	#var sz:Vector3 = data.placement_size
	#var radius : float = (rng.randf()) * \
		#(sz.x / 2.0) # i don't grok the div 2.0
	#var angle = rng.randf_range(0.0, 360.0)
	#return {
		#x = radius * cos(angle),
		#z = radius * sin(angle) }
#
#
## A little different from the rect and disc.  It returns a vec3
#static func rnd_surface(data:Dictionary)->Vector3:
	#var rng = data.rng
	#var sz:Vector3 = data.placement_size
	#var radius : float =  sz.x * 0.5
	#var x = rng.randf_range(-1,1)
	#var y = rng.randf_range(-1,1)
	#var z = rng.randf_range(-1,1)
	#var v3 = Vector3(x,y,z).normalized() * radius
	#return Vector3(v3.x, v3.y, v3.z)
#
#
### Very hacky atm. Not finished.
#static func reg_diamondish(data : Dictionary)->Dictionary:
	#var sz:Vector3 = data.placement_size
	#var i:int = data.i
	#var count:int = data.count
#
##	var shiftx := (sz.x / 2.0)
##	var shiftz := (sz.z / 2.0)
##	#D.say("sz.x: %s" % sz.x)
##	var pdivc = sz.x / count
##	var fi = i * pdivc
##	var x = fmod(fi, sz.x) - shiftx
##	var z = fmod(i * (sz.z/count), sz.z) - shiftz
##	D.say("x: %s" % x)
##	var regularpos := Vector3(
##		x,
##		0.0,
##		z,
##	)
	##Weirdly close and cool:
##	var shiftx := (sz.x * 2.0)
##	var shiftz := (sz.z * 2.0)
##	var regularpos := Vector3(
##		fmod(i, sz.x - (shiftx / count)),
##		0.0,
##		fmod(i, sz.z - (shiftz / count)),
##	)
	#var shiftx := (sz.x * 2.0)
	#var shiftz := (sz.z * 2.0)
	#var v3 := Vector3(
		#fmod(i, (sz.x - shiftx) / count),
		#0.0,
		#fmod(i, (sz.z - shiftz) / count),
	#) * count
	##D.say("regularpos: %s" % regularpos)
	##return Vector3(v3.x, v3.y, v3.z)
	#return {
		#x = v3.x,
		#z = v3.y }
#
#
### Very hacky atm. Not finished.
#static func reg_diagonals(data:Dictionary)->Dictionary:
#
	#var sz:Vector3 = data.placement_size
	#var i:int = data.i
	#var count:int = data.count
#
	#var shiftx := (sz.x * 2.0)
	#var shiftz := (sz.z * 2.0)
##	return Vector3(
##		fmod(i, sz.x - (shiftx / count)),
##		0.0,
##		fmod(i, sz.z - (shiftz / count)),
##	)
	#return {
		#x = fmod(i, sz.x - (shiftx / count)),
		#z = fmod(i, sz.z - (shiftz / count)) }
#
#
## ----------- Ray Direction Funcs ----------- #
#
#
#static func ray_up(data:Dictionary)->Dictionary:
	#var height:float = data.placement_size.y
	#return { from = -height / 2.0, to = height / 2.0 }
#
#
#static func ray_down(data:Dictionary)->Dictionary:
	#var height:float = data.placement_size.y
	#return { from = height / 2.0, to = -height / 2.0 }
#
#
#static var alternate:bool
#static func ray_up_down(data:Dictionary, from:Dictionary)->Dictionary:
	##var height:float = data.placement_size.y
	#var point = Vector2(from.x, from.z)
	## flip the return top bottom depending on modulo
	#var tmp = fmod(point.length_squared(),2.0)
	#alternate = !alternate
	#if alternate:#int(tmp):
		#return ray_up(data)
	#else:
		#return ray_down(data)
#
#static func ray_in_out(data, rnd_surface)->Dictionary:
	#var point = rnd_surface
	## flip the return top bottom depending on modulo
	#var tmp = fmod(point.length_squared(),2.0)
	#if int(tmp):
		#return {from = Vector3.ZERO, to = point}
	#else:
		#return {from = point, to = Vector3.ZERO}
