extends EditorInspectorPlugin

# MIT License
# Modified Work Copyright (c) 2023-present Donn Ingle.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## This file uses the other gd scripts in ui

var bg = ButtonGroup.new()

var _object

func _can_handle(object) -> bool:
	return true


func _parse_property(
	object: Object, type: Variant.Type,
	name: String, hint_type: PropertyHint,
	hint_string: String, usage_flags, wide: bool):
	hint_string = hint_string.to_lower()
	var split := hint_string.split(":")

	# Buttons
	if hint_string.begins_with("button"):
		#"hint_string":"button:Dab Dab Dab:main"
		var text := split[1]
		var style := split[2]
		var data := {name=name, text=text, style=style}
		add_custom_control(
			NSdabber.InspectorToolButton.new(object, data))
		return true #Returning true removes the built-in editor for this property

	# Icons
	#	ret.append({
	#		"name": &"orient_normal",
	#		"type": TYPE_NIL,
	#		"usage": PROPERTY_USAGE_EDITOR,
	#		"hint_string":"icon:assets/orient_normal.cleaned.svg:32x32"
	#	})
	if hint_string.begins_with("icon"):
		var icon_path := "res://addons/dabber/" + split[1]
		var size = split[2].split("x")
		size = Vector2(int(size[0]),int(size[1]))
		add_custom_control(NSdabber.InspectorIcon.new(icon_path, size))
		return true

	# Radio button
	# ret.append({
	# 	"name": &"orient_normal",
	# 	"type": TYPE_BOOL,
	# 	#"hint": 0,
	# 	"hint_string": "bool:radio:assets/orient_normal.cleaned.svg:32x32",
	# 	"usage": PROPERTY_USAGE_DEFAULT
	# })
	if hint_string.begins_with("bool:radio"):
		bg.allow_unpress = false
		_object = object
		#bool:radio:assets/orient_upright.cleaned.svg:32x32
		var icon_path
		var size
		if split.size()>2:
			icon_path = "res://addons/dabber/" + split[2]
			size = split[3].split("x")
			size = Vector2(int(size[0]),int(size[1]))

		var data = {name = name, bg = bg, icon_path = icon_path, size = size}
		var R = NSdabber.InspectorRadio.new(data)
		R.cb.button_pressed = _object.get(name)
		if not bg.is_connected(&"pressed", _button_group_pressed):
			bg.pressed.connect(_button_group_pressed, CONNECT_REFERENCE_COUNTED)
		add_custom_control(R)
		return true

	return false # else leave it

func _button_group_pressed(cb:CheckBox):
	_object._radio_toggled(cb.name)
