@tool
extends MarginContainer

var cb

func _init(data:Dictionary):
	var hbox := HBoxContainer.new()
	var spacer := Control.new()
	spacer.custom_minimum_size = Vector2(100,0)
	spacer.size_flags_horizontal = Control.SIZE_SHRINK_END
	cb = CheckBox.new()
	cb.toggle_mode = true
	cb.button_group = data.bg
	cb.text = data.name
	cb.name = data.name
	cb.icon = create_main_icon(data.icon_path, data.size)
	cb.size_flags_horizontal = Control.SIZE_SHRINK_BEGIN

	add_child(hbox)
	hbox.add_child(spacer)
	hbox.add_child(cb)


func create_main_icon(icon_path, size) -> Texture2D:
	var base_icon = load(icon_path) as Texture2D
	var image: Image = base_icon.get_image()
	image.resize(size.x, size.y, Image.INTERPOLATE_TRILINEAR)
	return ImageTexture.create_from_image(image)

