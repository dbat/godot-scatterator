@tool
extends MarginContainer

#const NS = preload("res://addons/dabber/src/ns.gd")


var styles = {basic = NSdabber.bStyleBasic, main = NSdabber.bStyleMain}


func _init(obj: Object, data:Dictionary):
	var button := Button.new()
	add_child(button)

	button.set("theme_override_styles/normal", styles[data.style])
	var margin:=5
	self.set("theme_override_constants/margin_left",margin)
	self.set("theme_override_constants/margin_top",margin)
	self.set("theme_override_constants/margin_right",margin)
	self.set("theme_override_constants/margin_bottom",margin)

	button.size_flags_horizontal = Control.SIZE_SHRINK_END
	button.text = "◽ " + data.text +  "◽"

	button.button_down.connect(obj._on_button_pressed.bind(data.name))

