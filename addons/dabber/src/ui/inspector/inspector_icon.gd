@tool
extends MarginContainer

func _init(icon_path, size):
	var icon := TextureRect.new()
	icon.texture = create_main_icon(icon_path, size)
	add_child(icon)
	icon.size_flags_horizontal = Control.SIZE_SHRINK_BEGIN


func create_main_icon(icon_path, size) -> Texture2D:
	var base_icon = load(icon_path) as Texture2D
	var image: Image = base_icon.get_image()
	image.resize(size.x, size.y, Image.INTERPOLATE_TRILINEAR)
	return ImageTexture.create_from_image(image)
