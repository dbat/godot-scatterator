@tool
class_name MultiMeshLibrary extends MultiMesh

## MultiMeshLibrary
##
## A Resource to supply a [MeshLibrary] to a MultimeshInstance3D
## as [param multimesh] where it will choose a mesh by
## [param mesh_index] to use as the [param mesh] value.
## [br]
## When used with a [dabDab] resource, [param use_all] can be
## toggled to override the specific [param mesh_index]. As such,
## all the meshes in the library will be supplied to the [dabDab]
## mesh_sources port. This lets you use [b]all[/b] the meshes
## within the library as mesh sources for a dab (scatter) process.


# MIT License
# Copyright (c) 2023 Donn Ingle (donn.ingle@gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

## The MeshLibrary you want to use.
@export var meshlib : MeshLibrary

## The index into the library of the mesh to use.[br]
## See also [param use_all]
@export var mesh_index : int = 0:
	set(i):
		var list := meshlib.get_item_list()
		if i not in range(0,list.size()):
			return
		mesh_index = i
		mesh = meshlib.get_item_mesh(i)

## If true, all the meshes in the library will be supplied.
@export var use_all:bool = false



func get_all_meshes_from_lib() -> Array[Mesh]:
	var all := meshlib.get_item_list()
	var ret : Array[Mesh]
	for id in all:
		ret.append(meshlib.get_item_mesh(id))
	return ret
