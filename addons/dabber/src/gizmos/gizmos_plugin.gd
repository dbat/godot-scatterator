@tool
extends EditorNode3DGizmoPlugin

# MIT License
# Original Work Copyright (c) 2020-present HungryProton.
# Modified Work Copyright (c) 2023-present Donn Ingle.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## This file serves mainly to forward the work on to gizmos.gd

const Gizmos = preload("res://addons/dabber/src/gizmos/gizmos.gd")

var _ed_settings := EditorInterface.get_editor_settings()


func _get_gizmo_name() -> String:
	return "dabGizmo3D"


func _init():
	create_material("main", Color(1, 0, 0))
	create_handle_material("handles")
	create_on_top_material("arrow_material", Color(1, 0.4, 0))
	#create_material("arrow_material", Color(1, 0.4, 0))
	#create_material("small_cursor_shape", Color.RED, false, true)
	create_on_top_material("small_cursor_shape", Color(1,1,1))
	var col = _ed_settings.get_setting(
		"interface/theme/accent_color").darkened(0.5)
	create_on_top_material("big_cursor_shape", col)
	create_handle_material("default_handle")

	#create_on_top_material()
	#create_icon_material("dab3dnode_billboard", NSdabber.DAB_3D_GIZMO_ICON, true)


func _create_gizmo(node):
	if node is NSdabber.dab3D:
		return Gizmos.new()
	else:
		return null


# Creates a standard material displayed on top of everything.
# Only exists because 'create_material() on_top' parameter doesn't seem to work.
func create_on_top_material(name: String, color := Color.WHITE,
		bmode := StandardMaterial3D.BLEND_MODE_ADD):
	var material := StandardMaterial3D.new()
	material.set_blend_mode(bmode)
	material.set_shading_mode(StandardMaterial3D.SHADING_MODE_PER_VERTEX)
	material.set_flag(StandardMaterial3D.FLAG_DISABLE_DEPTH_TEST, true)
	material.set_albedo(color)
	material.render_priority = 100
	add_material(name, material)


#func create_from_material_resource_file(resmaterial, name, color=null):
	#if color:
		#resmaterial.set_albedo(color)
	#resmaterial.render_priority = 100
	#add_material(name, resmaterial)




## App not needed anymore since using advanced plugin system:
## This func is called for every node added to the tree
## If it returns true, that node gets a gizmo.
## However, I noticed some seem to get more than one gizmo
## hence that test for the get_gizmos array and a return false
## I am saying, "look if this node already has a gizmo, then
## hey, all good? Right? Right?"
## NB: This DOES NOT RUN ON CLICK/SELECT OF A NODE ! ! !
#func _has_gizmo(node):
	#print("_HAS_GIZMO_RUNS?")
	#if node is NSdabber.dab3D:
		#var g = node.get_gizmos()
		#if g:
			#return false
	#return node is NSdabber.dab3D
