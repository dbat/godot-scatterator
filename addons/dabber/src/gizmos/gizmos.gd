@tool
extends EditorNode3DGizmo

# MIT License
# Original Work Copyright (c) 2020-present HungryProton.
# Modified Work Copyright (c) 2023-present Donn Ingle.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.



## This file is continued from gizmos_plugin.gd
## It's a per-node gizmo approach because I wanted the dabber icon to show
## in the editor. As of Godot 4.4 that's not working too well because
## the material is not showing on_top of the scene.





## Draw the gizmo
func _redraw():
	self.clear()
	var gizmos_node3d:dab3D = self.get_node_3d()

	if not gizmos_node3d is NSdabber.dab3D:
		return

	var aco := gizmos_node3d.active_cursor_object

	#add_unscaled_billboard(
			#get_plugin().get_material("dab3dnode_billboard", self),
			#0.1)

	# A default 'cursor' shape for the node
	#print("gizmos_node3d.active_cursor_object:", aco.name)
	if aco:
		var gizmo_mesh_dict : Dictionary = aco.gizmo_mesh
		self.add_mesh(gizmo_mesh_dict.mesh, get_plugin().get_material("small_cursor_shape", self))
		self.add_collision_triangles(gizmo_mesh_dict.tris)
		self.set_hidden(false)


	# If the large cursor is not visible, bail.
	if not gizmos_node3d.show_or_hide_dab_cursor:
		return

	var shapesize = gizmos_node3d.shapesize

	### CREATE the handles, one for each axis
	var handles := PackedVector3Array()
	var handles_material := get_plugin().get_material("default_handle", self)
	var gizmo_size : Dictionary

	# Place the handles - They all use three handles really
	gizmo_size = {
		right = Vector3.RIGHT * shapesize.x * 0.5,
		up    = Vector3.UP * shapesize.y * 0.5,
		back  = Vector3.BACK * shapesize.z * 0.5
	}
	handles.push_back(gizmo_size.right)
	handles.push_back(gizmo_size.up)
	handles.push_back(gizmo_size.back)

	self.add_handles(handles, handles_material, [])#, handles_ids)

	# Place the 'cursor' shape mesh
	var chosen_direction : int = gizmos_node3d.direction
	var chosen_target : int = gizmos_node3d.target
	#print("_redraw")
	#print("chosen_target:", chosen_target)

	var arrows:Array[NSdabber.CursorBase.Arrow]
	var cursorobj:NSdabber.CursorBase = gizmos_node3d.active_cursor_object
	arrows = cursorobj.get_arrows()
	#print("arrows:", arrows)
	var mesh : Mesh = cursorobj.cursor_mesh

	self.add_mesh(mesh, get_plugin().get_material("big_cursor_shape", self))

	### Draw the Arrows
	for arrow in arrows:
		self.add_mesh(arrow.mesh, get_plugin().get_material("arrow_material", self), arrow.t)



## This func is called PER HANDLE DRAGGED
enum AXIS {X,Y,Z}
func _set_handle(id: int, secondary: bool, camera: Camera3D, point: Vector2) -> void:
	# bail if weird handles
	if id < 0 or id > 2:
		return

	# Mysterious code from Hungry Proton.
	var axis := Vector3.ZERO
	axis[id] = 1.0 # handle 0:x, 1:y, 2:z

	var dab3d_node = self.get_node_3d()
	var gt := dab3d_node.get_global_transform()
	var gt_inverse := gt.affine_inverse()

	var origin := gt.origin
	var drag_axis := (axis * 4096) * gt_inverse
	var ray_from = camera.project_ray_origin(point)
	var ray_to = ray_from + camera.project_ray_normal(point) * 4096

	var points = Geometry3D.get_closest_points_between_segments(origin, drag_axis, ray_from, ray_to)

	var size = dab3d_node.shapesize
	size -= axis * size
	var dist = origin.distance_to(points[0]) * 2.0
	size += axis * dist

	dab3d_node.shapesize = dab3d_node.active_cursor_object.handle_drag_to_change_cursor_size(size, id)

	# magic line, makes the thing much smoother
	self.get_node_3d().update_gizmos()



## The "handlers" seem to be the little red circle grabby things.
func _get_handle_name(id: int, _secondary: bool) -> String:
	return ["X","Y","Z"][id]


## Fetch the size of the placement cursor from the NSdabber.dab3D node
func _get_handle_value(id: int, _secondary: bool) -> Variant:
	return self.get_node_3d().shapesize
