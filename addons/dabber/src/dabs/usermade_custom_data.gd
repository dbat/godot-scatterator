@tool
@icon("../../assets/icons/dabSettingMeshCustomData.small.svg")
class_name userMadeCustomDataResource
extends dabMeshCustomDataBase

## TODO
##
## Document me

# MIT License
# Copyright (c) 2023 Donn Ingle (donn.ingle@gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


static func rw_metadata():
	var _d = super()
	_d[&"display_class_name_as"] = &"Mesh Custom Data"
	_d[&"noinstance"] = false
	return _d


func matches():
	return [
		["res://automade_resources/extracts/quat/quaternius_pack.glb.BirchTree_1.tres", true, true],
		["orange", false, true]
	]

## Uses Color, but it's just a data vector.
func get_data(match_pathname:String, t:Transform3D, mesh_instance_index:int):
	#print("get_data args:", match_pathname, mesh_instance_index)
	match match_pathname:
		"res://automade_resources/extracts/quat/quaternius_pack.glb.BirchTree_1.tres":
			return Color(1,1,2,2)
		"orange":
			#do math with t etc.
			return Color(5,6,7,8)

	return null

func get_color(match_pathname:String, t:Transform3D, mesh_instance_index:int):
	match match_pathname:
		"res://automade_resources/extracts/quat/quaternius_pack.glb.BirchTree_1.tres":
			return Color.RED

	return null
