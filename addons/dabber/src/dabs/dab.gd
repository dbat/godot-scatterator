@tool
@icon("../../assets/icons/dabDab.small.svg")
class_name dabDab
extends Resource

## A single 'Dab'
##
## A dab is a resource to hold a bunch of details, like the meshes
## you want to use as well as their settings.
## [br]
## You append dabs to [dab3DResource] nodes, which go in your SceneTree.

# MIT License
# Copyright (c) 2023 Donn Ingle (donn.ingle@gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


## The metadata all goes into this method.
static func rw_metadata():
	return {
	&"display_class_name_as" : &"Dab",
	&"category" : &"Dabber Nodes",
	&"deny_list" : ["PackedScene"]
	}

const _VERBOSE := false


#-------------- Private vars ---------------#


var _last_mesh_sources:Array[Resource]
var _cached_meshes : Array[Mesh]
var _lock:=false


#---------------- Exports ------------------#


## Give this Dab a name to save your sanity.
@export var name : String:
	set(s):
		if s == "":
			name = "Dab" + str(abs(self.resource_path.hash() % 2048))
		else:
			name = s

## The initial number of dabs (you can multiply this
## from the dab3D node.)
@export_range(1, 100, 1, "or_greater") var count : int = 1

@export_group("Settings")

## The Position settings this dab will use.
@export var dab_settings_masking : NSdabber.dabSettingsMasking

## The Rotation settings this dab will use.
@export var dab_settings_rotation : NSdabber.dabSettingsRotation

## The Scale settings this dab will use.
@export var dab_settings_scale : NSdabber.dabSettingsScale

## Resources that contain meshes should be used.
## (When in doubt, re-noodling is how you force the cache to be rebuilt.)
@export var mesh_sources : Array[Resource]:
	set(arr):
		mesh_sources = arr
		if Engine.is_editor_hint():
			_cache_all_sources()
			_last_mesh_sources = mesh_sources.duplicate()
			if _VERBOSE: print("set arr:", arr)
	get:
		if _VERBOSE:
			print("---TOP--------")
			print("  mesh_sources:", mesh_sources)
			print("  TOP _last_mesh_sources:", _last_mesh_sources)
		if _last_mesh_sources != mesh_sources:
			if _VERBOSE:print("Arrays differ")
			# Think about it:
			# What we catch here, when we know the
			# two arrays differ, is the ONE item has been ADDED.
			# Only ONE --> will be in 'mesh_source_to_re_extract'
			# (If array item was REMOVED, then mesh_source_to_re_extract
			# will be null)
			var mesh_source_to_re_extract := mesh_sources.filter(
					func(r): return true if not r in _last_mesh_sources else false
			).pop_back()

			if mesh_source_to_re_extract:
				# There was a single addition, so only add that one
				if not _lock: # cannot fuck with self while in a "get"
					if _VERBOSE:print(" Cache one:", mesh_source_to_re_extract)
					_cache_one_source(mesh_source_to_re_extract)
			else:
				# Catching the array element that was removed is possible
				# but I am being lazy. So, just re-cache everything.
				if not _lock:
					_cache_all_sources()
			_lock = false # release the lock

			if _VERBOSE:
				print("      ---NE--------")
				print("      NEWB:", mesh_source_to_re_extract)
			_last_mesh_sources = mesh_sources.duplicate()
		return mesh_sources


## If provided, will take Custom Instance Data from here.
## See 'dabCustomDataBase' for more info.
@export var custom_data : NSdabber.dabMeshCustomDataBase


## Keep the dab pointing along the normal of the
## ray collision.
var orient_normal:bool = true

## Force the dab to point along the vector you supply.
var orient_to_vector:bool = false

## Vector to force dab to orient-towards.
var orient_vector: Vector3 = Vector3.UP

## A diameter for which a random wobble will happen.
var orient_wobble:float = 0.0

func _get_property_list():
	var ret = []
	ret.append({
		"name": &"orientation",
		"type": TYPE_NIL,
		"hint": &"orient",
		#"usage": PROPERTY_USAGE_CATEGORY
		"usage": PROPERTY_USAGE_GROUP,
	 })
	ret.append({
		"name": &"orient_wobble",
		"type": TYPE_FLOAT,
		"usage": PROPERTY_USAGE_DEFAULT,
		"hint_string": "0.0,10.0,or_greater",
		"hint": PROPERTY_HINT_RANGE,
	 })
	ret.append({
		"name": &"orient_normal",
		"type": TYPE_BOOL,
		#"hint": 0,
		"hint_string": "bool:radio:assets/icons/orient_normal.cleaned.svg:32x32",
		"usage": PROPERTY_USAGE_DEFAULT
	})
	ret.append({
		"name": &"orient_to_vector",
		"type": TYPE_BOOL,
		"hint": PROPERTY_HINT_TYPE_STRING,
		"hint_string": "bool:radio:assets/icons/orient_upright.cleaned.svg:32x32",
		"usage": PROPERTY_USAGE_DEFAULT
	})
	var use := PROPERTY_USAGE_DEFAULT
	if orient_normal:# or not orient_to_vector:
		use = PROPERTY_USAGE_NO_EDITOR
	ret.append({
		"name": &"orient_vector",
		"type": TYPE_VECTOR3,
		"hint": PROPERTY_HINT_TYPE_STRING,
		"hint_string": "Vector3",
		"usage": use
	})
	return ret


#---------------- Signal Handler ------------------#

# Signal handler from inspector_customize_plugin.gd
# Basic button press.
func _radio_toggled(name):
	match name:
		&"orient_normal":
			orient_normal = true
			orient_to_vector = false
		&"orient_to_vector":
			orient_to_vector = true
			orient_normal = false
	notify_property_list_changed()


#---------------- Private Funcs ------------------#

func _cache_all_sources():
	_lock = true
	_cached_meshes.clear()
	for mesh_resource in mesh_sources:
		_cache_one_source(mesh_resource)


func _cache_one_source(mesh_resource):
	_lock = true
	var meshes:Array[Mesh] = _fetch_meshes_from_source(mesh_resource)
	for msh in meshes:
		if (msh) and (msh not in _cached_meshes):
			_cached_meshes.append(msh)


func _fetch_meshes_from_source(mesh_resource:Resource) -> Array[Mesh]:
	if mesh_resource is ArrayMesh:
		return [mesh_resource]

	if mesh_resource is PrimitiveMesh:
		return [mesh_resource]

	# should be blocked by deny metadata, but in case...
	if mesh_resource is PackedScene:
		#print("Can't use packed scenes directly")
		return []

	if mesh_resource is MultiMeshLibrary:
		if not mesh_resource.use_all:
			return [mesh_resource.meshlib.get_item_mesh(mesh_resource.mesh_index)]
		else:
			return mesh_resource.get_all_meshes_from_lib()

	if mesh_resource is NSdabber.dabPathFilter:
		var paths = mesh_resource.get_filtered_files()
		#print("HERE:", paths)
		return _get_MESHES_from_list_of_paths(paths)

	if mesh_resource is NSdabber.dabFolder:
		return _get_MESHES_from_DIRECTORY(mesh_resource.path)

	#if mesh_resource is NSdabber.dabPackedsceneExtractor:
		#return _get_MESHES_from_DIRECTORY(mesh_resource.path)
	return []


func _get_MESHES_from_DIRECTORY(mesh_path) -> Array[Mesh]:
	var ret:Array[Mesh]
	# This next arcane line uses an obscure class that will let me detect
	# the type of a resource from the file path.
	# NB: EditorInterface will not work at runtime.
	var dir = EditorInterface.get_resource_filesystem().get_filesystem_path(mesh_path)
	if dir:
		for i_file in range(0,dir.get_file_count()):
			var paf := dir.get_file_path(i_file)
			# Here is the type detection:
			if dir.get_file_type(i_file) == &"ArrayMesh":
				var msh:Mesh = ResourceLoader.load(paf) #TODO what errors can happen?
				ret.append(msh)
	return ret


func _get_MESHES_from_list_of_paths(paths) -> Array[Mesh]:
	var ret:Array[Mesh]
	if Engine.is_editor_hint():
		var D = EditorInterface.get_resource_filesystem()
		for paf in paths:
			# This next arcane line uses an obscure class that will let me detect
			# the type of a resource from the file path.
			var dir = D.get_filesystem_path(paf.get_base_dir())
			if dir:
				var i_file := dir.find_file_index(paf.get_file()) #TODO what errors can happen?
				# Here is the type detection:
				if dir.get_file_type(i_file) == &"ArrayMesh":
					var msh:Mesh = ResourceLoader.load(paf) #TODO what errors can happen?
					ret.append(msh)
	return ret


#---------------- Public Func ------------------#

## Called from dab3D Node, only while in-editor.
func get_cached_meshes() -> Array[Mesh]:
	if Engine.is_editor_hint():
		_cache_all_sources()
	return _cached_meshes
