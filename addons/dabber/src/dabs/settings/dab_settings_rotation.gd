@tool
@icon("../../../assets/icons/dabSettingRotation.small.svg")
class_name dabSettingsRotation
extends dabSettingsBase

# MIT License
# Copyright (c) 2023 Donn Ingle (donn.ingle@gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

## Rotation Settings for a Dab
##
## Set the properties on this to influence how the dab will rotate its meshes.


## The metadata all goes into this method.
static func rw_metadata():
	return {
	&"display_class_name_as" : &"Rotation Settings",
	&"category" : &"Dabber Nodes"
	}


#----------------------------------------------#


@export_group("Axes Random")

## Will randomly rotate on all three local axes.
## This overrides the individual axis choices below.
@export var random_axes:bool = false:
	set(b):
		random_axes = b
		if b:
			X_axis = false
			Y_axis = false
			Z_axis = false

## Rotate randomly around local X.
@export var X_axis:bool = false:
	set(b):
		X_axis = b
		if b: random_axes = false

## Rotate randomly around local Y.
@export var Y_axis:bool = false:
	set(b):
		Y_axis = b
		if b: random_axes = false

## Rotate randomly around local Z.
@export var Z_axis:bool = false:
	set(b):
		Z_axis = b
		if b: random_axes = false

## Random amount in degrees.
@export var random_degrees : float = 0.0

@export_group("Texture Override")

## If you have a texture, rotation is taken from the luminance
## value of each pixel. The random settings above are ignored.
@export var texture : Texture2D:
	set(tex):
		texture = tex
		_images_cached = false # only flag it, do nothing else

@export_group("Gradient Textures")

## A texture to influence rotation along the X axis. Only the
## first row will be used, so make it 1px high.
@export var texture_X : Texture2D:
	set(tex):
		texture_X = tex
		_images_cached = false # only flag it, do nothing else

## A texture to influence rotation along the Y axis. Only the
## first row will be used, so make it 1px high.
@export var texture_Y : Texture2D:
	set(tex):
		texture_Y = tex
		_images_cached = false # only flag it, do nothing else

## A texture to influence rotation along the Z axis. Only the
## first row will be used, so make it 1px high.
@export var texture_Z : Texture2D:
	set(tex):
		texture_Z = tex
		_images_cached = false # only flag it, do nothing else


var _cached_rot_image : Image
var _cached_rot_image_X : Image
var _cached_rot_image_Y : Image
var _cached_rot_image_Z : Image
var _images_cached := false


func _cache_Images() -> void:
	_cached_rot_image = self.get_image_or_null(&"texture")
	if texture_X:
		_cached_rot_image_X = self.get_image_or_null(&"texture_X")
	if texture_Y:
		_cached_rot_image_Y = self.get_image_or_null(&"texture_Y")
	if texture_Z:
		_cached_rot_image_Z = self.get_image_or_null(&"texture_Z")
	_images_cached = true


func mess_with(t, v_probe, placement_size, _rng) -> Transform3D:
	#Cache all the Images involved.
	if not _images_cached: _cache_Images()

	var all_bases=[t.basis.x, t.basis.y, t.basis.z]
	var selected_axes = all_bases
	var extra_radians:float = 0.0 #starts exactly at zero
	if _cached_rot_image:
		var v = dabUtils.get_luminance(
				_cached_rot_image, v_probe.local_ray_to, placement_size)
		#print("got v:", v)
		extra_radians = v * TAU
	if random_axes:
		selected_axes = all_bases
	else:
		var yaxis = all_bases[1] if Y_axis else null
		var zaxis = all_bases[2] if Z_axis else null
		var xaxis = all_bases[0] if X_axis else null
		selected_axes = [xaxis,yaxis,zaxis]
	selected_axes = selected_axes.filter(func(a):return a != null)
	for axis in selected_axes:
		var rad:float
		if extra_radians == 0.0: #which means no texture was set
			#use the random_degrees, if > zero
			if random_degrees > 0.0:
				rad = deg_to_rad(
					_rng.randf_range(
						-random_degrees,
						 random_degrees)
					)
		else:
			# Use the texture and the various axis choices
			rad = extra_radians
		t = t.rotated(axis, rad)

	#-- Apply the Rot Gradients XYZ if any --
	#print("_cached_rot_image_X:", _cached_rot_image_X)
	if _cached_rot_image_X:
		var p = v_probe.local_ray_to.x
		var _lum = dabUtils.get_luminance_from_top_row(
			_cached_rot_image_X, p, placement_size.x)
		_lum = remap(_lum, 0, 1, 0, TAU)
		#print(_lum, " ", selected_axes)
		for axis in selected_axes:
			t = t.rotated(axis, _lum)

	if _cached_rot_image_Y:
		var p = v_probe.local_ray_to.y
		var _lum = dabUtils.get_luminance_from_top_row(
			_cached_rot_image_Y, p, placement_size.y)
		_lum = remap(_lum, 0, 1, 0, TAU)
		#print(_lum)
		for axis in selected_axes:
			t = t.rotated(axis, _lum)

	if _cached_rot_image_Z:
		var p = v_probe.local_ray_to.z
		var _lum = dabUtils.get_luminance_from_top_row(
			_cached_rot_image_Z, p, placement_size.z)
		_lum = remap(_lum, 0, 1, 0, TAU)
		#print(_lum)
		for axis in selected_axes:
			t = t.rotated(axis, _lum)

	return t
