@tool
@icon("../../../assets/icons/dabSettingRotation.small.svg")
class_name dabSettingsScale
extends dabSettingsBase

# MIT License
# Copyright (c) 2023 Donn Ingle (donn.ingle@gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

## Scale Settings for a Dab
##
## Set the properties on this to influence how the dab will scale its meshes.


## The metadata all goes into this method.
static func rw_metadata():
	return {
	&"display_class_name_as" : &"Scale Settings",
	&"category" : &"Dabber Nodes"
	}

#----------------------------------------------#

@export_group("Axes")

## Will randomly scale on all three local axes.
## Ignores the axes chosen below.
@export var random_axes:bool = false:
	set(b):
		random_axes = b
		if b:
			X_axis = false
			Y_axis = false
			Z_axis = false

## Scale along local X.
@export var X_axis:bool = false:
	set(b):
		X_axis = b
		if b: random_axes = false

## Scale along local Y.
@export var Y_axis:bool = false:
	set(b):
		Y_axis = b
		if b: random_axes = false

## Scale along local Z.
@export var Z_axis:bool = false:
	set(b):
		Z_axis = b
		if b: random_axes = false


@export_group("Amount")

## If you have a texture, only it is used for scale.
@export var texture : Texture2D:
	set(tex):
		texture = tex
		_images_cached = false # only flag it, do nothing else

## Will randomly scale by this amount along the chosen axes.
## Ignored if you have a texture.
@export var random_size : float = 1.0:
	set(f):
		random_size = f
		if f == 0.0:
			random_size = 1.0

## You can tweak the scale up or down with this.
@export var magnitude : float = 1.0


@export_group("Gradients")
## A texture to influence scale along the X axis. Only the
## first row will be used, so make it 1px high.
@export var texture_X : Texture2D:
	set(tex):
		texture_X = tex
		_images_cached = false # only flag it, do nothing else

## A texture to influence scale along the Y axis. Only the
## first row will be used, so make it 1px high.
@export var texture_Y : Texture2D:
	set(tex):
		texture_Y = tex
		_images_cached = false # only flag it, do nothing else

## A texture to influence scale along the Z axis. Only the
## first row will be used, so make it 1px high.
@export var texture_Z : Texture2D:
	set(tex):
		texture_Z = tex
		_images_cached = false # only flag it, do nothing else


var _cached_scale_image:Image
var _cached_scale_image_Y:Image
var _cached_scale_image_Z:Image
var _cached_scale_image_X:Image
var _images_cached := false

func _cache_Images() -> void:
	_cached_scale_image = self.get_image_or_null(&"texture")
	if texture_X:
		_cached_scale_image_X = self.get_image_or_null(&"texture_X")
	if texture_Y:
		_cached_scale_image_Y = self.get_image_or_null(&"texture_Y")
	if texture_Z:
		_cached_scale_image_Z = self.get_image_or_null(&"texture_Z")
	_images_cached = true


func mess_with(t, v_probe, placement_size, _rng) -> Transform3D:
	#Cache all the Images involved.
	if not _images_cached: _cache_Images()

	var scale_by:Vector3 = Vector3.ONE
	var rnd_vector : Vector3
	if _cached_scale_image:
		# use the texture for scale
		var v = dabUtils.get_luminance(
				_cached_scale_image, v_probe.local_ray_to, placement_size)
		rnd_vector = Vector3(v,v,v)
	else:
		# use the random scale instead
		rnd_vector = Vector3.ONE * \
			_rng.randf_range(0, random_size)

	if random_axes:
		# Randomize all the axes
		var xaxis = _rng.randf_range(0, rnd_vector.x) # 0 or something
		var yaxis = _rng.randf_range(0, rnd_vector.y)
		var zaxis = _rng.randf_range(0, rnd_vector.z)
		scale_by = Vector3(xaxis, yaxis, zaxis)
		#D.say(" texture and rnd all scale_by: %s" % scale_by)
	else:
		var _x = int(X_axis)* rnd_vector.x
		var _y = int(Y_axis)* rnd_vector.y
		var _z = int(Z_axis)* rnd_vector.z
		_x = 1 if is_zero_approx(_x) else _x
		_y = 1 if is_zero_approx(_y) else _y
		_z = 1 if is_zero_approx(_z) else _z
		# Use the chosen axes "scale_random_size"\
		scale_by = Vector3(_x,_y,_z)

	scale_by = scale_by * magnitude

	# Make sure the parts are > 0
	# No negative scale for now.
	scale_by = Vector3(
		scale_by.x if scale_by.x > 0.0 else 1.0,
		scale_by.y if scale_by.y > 0.0 else 1.0,
		scale_by.z if scale_by.z > 0.0 else 1.0
		)
	#D.say("scale_by: %s" % scale_by)
	t = t.scaled(scale_by)

	#-- Apply the Scale Gradients XYZ if any --
	if _cached_scale_image_X:
		var p = v_probe.local_ray_to.x
		var _lum = dabUtils.get_luminance_from_top_row(
			_cached_scale_image_X, p, placement_size.x)
		scale_by = Vector3(_lum,1,1)
		t = t.scaled(scale_by)

	if _cached_scale_image_Y:
		var p = v_probe.local_ray_to.y
		var _lum = dabUtils.get_luminance_from_top_row(
			_cached_scale_image_Y, p, placement_size.y)
		scale_by = Vector3(1,_lum,1)
		t = t.scaled(scale_by)

	if _cached_scale_image_Z:
		var p = v_probe.local_ray_to.z
		var _lum = dabUtils.get_luminance_from_top_row(
			_cached_scale_image_Z, p, placement_size.z)
		scale_by = Vector3(1,1,_lum)
		t = t.scaled(scale_by)

	return t
