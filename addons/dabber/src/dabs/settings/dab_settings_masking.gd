@tool
@icon("../../../assets/icons/dabSettingGroups.small.svg")
class_name dabSettingsMasking
extends dabSettingsBase

# MIT License
# Copyright (c) 2023 Donn Ingle (donn.ingle@gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

## Masking Settings for a Dab
##
## Set the properties on this to influence how the dab will mask/cull its meshes.
## [br][br]
## [b]Overlapping[/b]
## [br]
## [param avoid_overlapping] enables a (slow) attempt to prevent overlapping meshes.
## [br]
## [param margin] will grow/reduce the [AABB] of the overlap test.
## [br][br]
## [b]Group Masks[/b]
## [br]
## You can choose groups to avoid or prefer while the dabbing happens. For example
## a mesh in a group called "pathway", with that name ("pathway") in the [param avoid]
## array, will not have any meshes dabbed onto it.
## [br][br]
## [b]Image Mask[/b]
## [br]
## An image can guide where dabs will go. The luminosity of each pixel will
## determine whether a mesh is placed or not.


## The metadata all goes into this method.
static func rw_metadata():
	return {
	&"display_class_name_as" : &"Masking Settings",
	&"category" : &"Dabber Nodes"
	}

var _SLOT_SCENE:Control
var _att_groups_array_size:int
var _rep_groups_array_size:int


#@export_group("Offset")

### Add an offset to the placed instances.
#@export var offset : Vector3 = Vector3.ZERO

@export_group("Overlapping")

## When on, overlapping will be avoided by the margin.
@export var avoid_overlapping:bool = false

## Avoid overlapping by this amount (can be negative).
@export_range(-2000.0, 2000.0, 0.001, "or_greater", "or_less") var margin : float = 0.0

@export_group("Group Masks")

## Group names that allow dabbing.
@export var attractor_groups : Array[StringName]:
	set(arr):
		attractor_groups = arr
		_att_groups_array_size = attractor_groups.size()
	get:
		if attractor_groups.size() != _att_groups_array_size:
			_att_groups_array_size = attractor_groups.size()
			_redraw()
		return attractor_groups

## Group names that deny dabbing.
@export var repeller_groups : Array[StringName]:
	set(arr):
		repeller_groups = arr
		_rep_groups_array_size = repeller_groups.size()
	get:
		if repeller_groups.size() != _rep_groups_array_size:
			_rep_groups_array_size = repeller_groups.size()
			_redraw()
		return repeller_groups

@export_group("Image Mask")

## Placement/position texture. Greyscale is best.
@export var texture : Texture2D

## Adjust how effective the texture is.
@export var texture_cutoff : float = 0.5


#----------------------------------------------#


func show_node_gui(graphnode:GraphNode, SCENE_PRELOAD) -> void:
	_SLOT_SCENE = graphnode.get_node_or_null("Extension")

	if not _SLOT_SCENE:
		_SLOT_SCENE = NSdabber.PREF_AVOID_SCENE.instantiate()
		graphnode.add_child(_SLOT_SCENE)
	_redraw()

func _redraw():
	var _list : ItemList
	if _SLOT_SCENE:
		_SLOT_SCENE.get_node("box/pref").propagate_call("set_visible",[!attractor_groups.is_empty()])
		_list = _SLOT_SCENE.get_node("%prefer")
		_list.clear()

		for g in attractor_groups:
			_list.add_item(g)

		_SLOT_SCENE.get_node("box/avoid").propagate_call("set_visible",[!repeller_groups.is_empty()])
		_list = _SLOT_SCENE.get_node("%avoid")
		_list.clear()

		for g in repeller_groups:
			_list.add_item(g)

func has_attractor()->bool:
	return attractor_groups.size() > 0

func get_repel_groups()->Array:
	if repeller_groups and \
			repeller_groups.size() > 0:
		return repeller_groups
	return []

func get_attract_groups()->Array:
	if attractor_groups and \
			attractor_groups.size() > 0:
		return attractor_groups
	return []
