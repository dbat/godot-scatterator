@tool
class_name dabMeshCustomDataBase
extends Resource

## Base class for Custom Meshinstance Data and Color
##
## [b]NB:[/b] Make a new script that extends me. I am useless on my own.
## [br][br]
## Override the 'matches' func and return an array of sub-arrays that describe
## what named mesh files (paths) will have data and/or color data.
## [br][br]
## The format is [pathname:String, data:bool, color:bool] [br]
## e.g. return [ ["res://meshes/foo.tres", true, true], ["res://meshes/frotz.tres", false, true]]


# MIT License
# Copyright (c) 2023 Donn Ingle (donn.ingle@gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.




## The metadata all goes into this method.
static func rw_metadata():
	return {
	&"category" : &"Dabber Nodes",
	&"noinstance" : true
	}


## Return an array of sub-arrays that describe what named mesh paths
## will have data and/or color data.[br]
## It's [pathname:String, data:bool, color:bool] [br]
## e.g. return [ ["res://meshes/foo.tres", true, true], ["res://meshes/frotz.tres", false, true]]
func matches(): return []


func uses_data(match_pathname):
	var _l:Array = matches()
	var _n = _l.filter(func(_t): return _t[0] == match_pathname)
	if _n:
		return _n[0][1]

func uses_color(match_pathname):
	var _l = matches()
	var _n = _l.filter(func(_t): return _t[0] == match_pathname)
	if _n:
		return _n[0][2]

## Override this func to get custom data per instance.
## You need to return a Vector4 or null.
func get_data(match_pathname:String, t:Transform3D, mesh_instance_index:int):
	pass

## Override this func to get custom color per instance.
## You need to return a Color, or null.
func get_color(match_pathname:String, t:Transform3D, mesh_instance_index:int):
	pass
