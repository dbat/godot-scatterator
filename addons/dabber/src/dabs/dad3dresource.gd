@tool
@icon("../../assets/icons/dab3dResource.small.svg")
class_name dab3DResource
extends Resource

# MIT License
# Copyright (c) 2023 Donn Ingle (donn.ingle@gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

## A resource which holds everything that goes into making a 'Dab'.
##
## Place this resource into a [dab3D] Node in order to scatter it onto
## your geometry etc.


## The metadata all goes into this method.
static func rw_metadata():
	return {
	&"display_class_name_as" : &"Dab3D Resource",
	&"category" : &"Dabber Nodes"
	}

const _VERBOSE := false


#----------------------------------------------#


## A name to help you recognize this dab3DResource.
@export var name:String:
	set(s):
		if s:
			name = s

## Add all the dabs you wish to deploy.
@export var dabs : Array[NSdabber.dabDab]


# Disabled Sept 2024. TODO
### If you use a mix texture, each 'dab' will be
### distributed across the texture by their index
### in the dabs array.
#@export var mix_dabs_texture : Texture2D
#
#
### How much to mingle the edges of the mix texture by.
#@export_range(0, 100.0, 1, "or_greater") var mix_mingle : float = 1.0
