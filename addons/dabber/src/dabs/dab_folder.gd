@tool
@icon("../../assets/icons/dabSettingFolder.small.svg")
class_name dabFolder
extends Resource

## A Path to a folder
##
## Useful for connecting an entire path to a [param mesh_sources] port on a [dabDab] node,
## or into a [dabPathFilter] node.

# MIT License
# Copyright (c) 2023 Donn Ingle (donn.ingle@gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

## The metadata all goes into this method:
static func rw_metadata():
	return {
	&"display_class_name_as" : &"Folder",
	&"category" : &"Dabber Nodes"
	}

const _VERBOSE := true

#---------------- exports ------------------#

## Give this a name to save your sanity.
@export var name : String:
	set(s):
		if s == "" and path != "":
			name = path
		elif s == "":
			name = "No Path"
		else:
			name = s
		# Doing this causes a changed emit...
		#   self.resource_name = name
		# This causes Bad Thing To Happen (TM)
		# therefore, let's not do that.

## Choose a directory where resources are.
@export_dir var path:String:
	set(p):
		path = p
		if not name:
			name = p
