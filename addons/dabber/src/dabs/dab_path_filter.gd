@tool
@icon("res://addons/dabber/assets/icons/dabNameFilter.small.svg")
class_name dabPathFilter
extends Resource

# MIT License
# Copyright (c) 2023 Donn Ingle (donn.ingle@gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

## Path Filter
##
## Takes any Resource that provides a [param path] property
## (for example one or more [dabFolder]s) and
## filters that path against two lists:[br]
## [param prefer] (only these)—only matches in this list will be allowed through.
## [br]
## [param avoid] (never these)—anything in this list will be blocked.
## [br][br]
## Connect the output to nodes to any [i]mesh_source(s)[/i] port; for example in a [dabDab] node.


## The metadata all goes into this method.
static func rw_metadata():
	return {
	&"display_class_name_as" : &"Path Filter",
	&"category" : &"Dabber Nodes",
	&"deny_list" : ["PackedScene", "dabPathFilter"]
	}

const _VERBOSE := false

var _SLOT_SCENE:Control


#---------------- exports ------------------#

## Supply any Resource with a 'path' property.
@export var paths:Array[Resource]

## [b]Only These[/b]—only matches in this list will be allowed through.
@export_file var prefer : Array[String]:
	set(arr):
		prefer = arr
		_prefdup = prefer.duplicate()
	get:
		if prefer != _prefdup:
			_prefdup = prefer.duplicate()
			if _VERBOSE:
				print("change in prefer filters array:", prefer)
			_redraw()
			#emit_changed()
		return prefer

## [b]Never These[/b]—anything in this list will be blocked.
@export_file var avoid : Array[String]:
	set(arr):
		avoid = arr
		_avoidup = avoid.duplicate()
	get:
		if avoid != _avoidup:
			_avoidup = avoid.duplicate()
			if _VERBOSE:
				print("change in avoid filters array:", avoid)
			_redraw()
			#emit_changed()
		return avoid


#-------------- Private vars ---------------#

var _prefdup : Array[String]
var _avoidup : Array[String]

var _preflist : ItemList
var _avoidlist : ItemList


#---------------- Funcs ------------------#


## Do we prefer/avoid this path arg?
func _skip_file(paf) -> bool:
	var skip_paf := not prefer.is_empty()
	if paf in prefer: skip_paf = false
	if paf in avoid: skip_paf = true
	if _VERBOSE and skip_paf: print("skip:", paf)
	return skip_paf


## Called from dabDab to pump all my filtered paths out.
func get_filtered_files() -> Array[String]:
	var ret:Array[String]
	for path_res in paths:
		# NB: Keep in mind Engine.is_editor_hint() because
		# EditorInterface can ONLY BE USED in the EDITOR (i.e not runtime)
		var dir = EditorInterface.get_resource_filesystem().get_filesystem_path(path_res.path)
		if dir:
			for i_file in range(0,dir.get_file_count()):
				var paf := dir.get_file_path(i_file)
				if not _skip_file(paf):
					ret.append(paf)
	return ret


## If the gui is not instanced, do so. Add it to the node parent.
func show_node_gui(graphnode:GraphNode, SCENE_PRELOAD) -> void:
	_SLOT_SCENE = graphnode.get_node_or_null("Extension")
	if not _SLOT_SCENE:
		_SLOT_SCENE = NSdabber.PREF_AVOID_SCENE.instantiate()
		graphnode.add_child(_SLOT_SCENE)
	_redraw()


func _redraw():
	if _SLOT_SCENE:
		_SLOT_SCENE.get_node("box/pref").propagate_call("set_visible",[!prefer.is_empty()])
		_preflist = _SLOT_SCENE.get_node("%prefer")
		_preflist.clear()
		for n in prefer:
			var i := _preflist.add_item(n.get_file())

		_SLOT_SCENE.get_node("box/avoid").propagate_call("set_visible",[!avoid.is_empty()])
		_avoidlist  = _SLOT_SCENE.get_node("%avoid")
		_avoidlist.clear()
		for n in avoid:
			var i := _avoidlist.add_item(n.get_file())
