#@tool
#@icon("res://addons/dabber/assets/icons/dabDab.small.svg")
#class_name dabDab
#extends Resource
#
#const _rwMetadata = {
	#"display_class_name_as" : &"Dab",
	#"category" : &"Dabber Nodes"
#}
#
##const NSdabber = preload("res://addons/dabber/src/ns.gd")
#
#const VERBOSE := false
#
##---------------- exports ------------------#
#
### Give this Dab a name to save your sanity.
#@export var name : String:
	#set(s):
		#if s == "":
			#name = "Dab" + str(abs(self.resource_path.hash() % 2048))
		#else:
			#name = s
#
		## Doing this causes a changed emit...
		##   self.resource_name = name
		## This causes Bad Thing To Happen (TM)
		## therefore, let's not do that.
#
#
#### Choose a directory where you mesh resources can be
#### saved to.
##@export_dir var extract_path:String
#
#
### The initial number of dabs (you can multiply this
### from the dab3D node)
#@export_range(1, 100, 1, "or_greater") var count : int = 1
#
#
### Avoid overlapping by this amount (1:1 meter). Zero means it won't even try.
#@export_range(0.0, 2000.0, 0.001, "or_greater") var overlap : float = 0.0
#
#
### THe settings this dab will use
#@export var dab_settings : Array[NSdabber.dabSetting]
#
#
### If you want custom data per instance, create a resource that
### has a "func get_data(t:Transform3D, index:int)->Vector4:"
### It's up to you as to what you want to return.
#@export var custom_data : Resource
#
#
### Resources that contain meshes should be used.
### Pulling a source node out (disconnecting) and reconnecting
### is how you force the cache to be built again from scratch.
### If you alter a PackedScene, then unplug and re-plug it in RW
### to get the meshes properly updated again.
#@export var mesh_sources : Array[Resource]:
	#set(arr):
		#mesh_sources = arr
		#_last_mesh_sources = mesh_sources.duplicate()
		#if VERBOSE: print("set arr:", arr)
	#get:
		#if VERBOSE:
			#print("---TOP--------")
			#print("  mesh_sources:", mesh_sources)
			#print("  TOP _last_mesh_sources:", _last_mesh_sources)
		#if _last_mesh_sources != mesh_sources:
			#if VERBOSE:
				#print("Arrays differ")
			## Think about it: what we catch here, when we know the
			## two arrays differ, is that ONE item  has been ADDED or RMd
			## Only ONE --> will be in 'mesh_source_to_re_extract'
			#mesh_source_to_re_extract = mesh_sources.filter(
					#func(r): return true if not r in _last_mesh_sources else false
			#).pop_back()
			#if VERBOSE:
				#print("      ---NE--------")
				#print("      NEWB:", mesh_source_to_re_extract)
#
			## nb: do this before the emit!
			#_last_mesh_sources = mesh_sources.duplicate()
#
			## Then tell upstream that I have changed.
			#emit_changed()
			#mesh_source_to_re_extract = null
		#return mesh_sources
#
#
##----------------------------------------------#
#var mesh_source_to_re_extract:Resource
#
#
##----------------------------------------------#
#var _last_mesh_sources:Array[Resource]
