#@tool
#@icon("res://addons/dabber/assets/icons/dabNameFilter.small.svg")
#class_name dabMeshSourceFilter
#extends Resource
#
#const _rwMetadata = {
	#"display_class_name_as" : &"Mesh Source Filter",
	#"category" : &"Dabber Nodes"
#}
#
#
#const VERBOSE := true
#const GROUPSCENE = preload("name_filter_ui.tscn")
#
#var SLOT_SCENE:Control
#
### Which PackedScene to use
#@export var packed_scene:PackedScene
#
#var preview_this:
	#get:
		#return packed_scene
#
### NodePaths to prefer from the input PackedScene
#@export var prefer : Array[NodePath]:
	#set(arr):
		#prefer = arr
		#_prefdup = prefer.duplicate()
	#get:
		#if prefer != _prefdup:
			#_prefdup = prefer.duplicate()
			#if VERBOSE:
				#print("change in prefer filters array:", prefer)
			#_redraw()
			#emit_changed()
		#return prefer
#
### NodePaths to avoid from the input PackedScene
#@export var avoid : Array[NodePath]:
	#set(arr):
		#avoid = arr
		#_avoidup = avoid.duplicate()
	#get:
		#if avoid != _avoidup:
			#_avoidup = avoid.duplicate()
			#if VERBOSE:
				#print("change in avoid filters array:", avoid)
			#_redraw()
			#emit_changed()
		#return avoid
#
#var _prefdup : Array[NodePath]
#var _avoidup : Array[NodePath]
#
#var _preflist : ItemList
#var _avoidlist : ItemList
#
### If the gui is not instanced, do so. Add it to the node parent
#func show_node_gui(graphnode:GraphNode, SCENE_PRELOAD) -> void:
	#SLOT_SCENE = graphnode.get_node_or_null("Extension")
	#if not SLOT_SCENE:
		#SLOT_SCENE = GROUPSCENE.instantiate()
		#graphnode.add_child(SLOT_SCENE)
	#_redraw()
#
#func _redraw():
	#if SLOT_SCENE:
		#_preflist = SLOT_SCENE.get_node("HBoxContainer/pref/slotvalue")
		#_preflist.clear()
		#for n in prefer:
			#_preflist.add_item(n)
#
		#_avoidlist  = SLOT_SCENE.get_node("HBoxContainer/avoid/slotvalue")
		#_avoidlist.clear()
		#for n in avoid:
			#_avoidlist.add_item(n)
