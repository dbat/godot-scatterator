#@tool
##@icon("res://addons/dabber/assets/icons/dabDab.small.svg")
#class_name dabFolder
#extends Resource
#
### New dev as of April 2024
### Trying to remove packedscenes in favour of dirs of meshes
### extracted in the import process
###
### Ok, packedscenes are in fact cool -- because I extract all the
### meshes anyway.
### Fine, they will export to final game, so there will be a doubling
### of the mesh data, unless we remember to delete it before export,
### BUT if I use a glb directly (drop in as a node) it is a packedscene
### and I am told that on export glb's will be ignored. This means at least
### the double size thing will go away - but, will it cause ref errors to the
### glb that is no more? The plugin is not suppoed to run at run time anyway.
###
### But, this "folder" resource needs to exist too. Just in case.
### So, it must take a path param and scan for legitimate mesh files.
#const _rwMetadata = {
	#"display_class_name_as" : &"Folder",
	#"category" : &"Dabber Nodes"
#}
#
#const VERBOSE := false
#
##---------------- exports ------------------#
#
### Give this a name to save your sanity.
#@export var name : String:
	#set(s):
		#if s == "" and path != "":
			#name = path
		#elif s == "":
			#name = "No Path"
		#else:
			#name = s
		## Doing this causes a changed emit...
		##   self.resource_name = name
		## This causes Bad Thing To Happen (TM)
		## therefore, let's not do that.
#
#
### Choose a directory where shit is
#@export_dir var path:String:
	#set(p):
		#path = p
		#if not name:
			#name = p
		#emit_changed()
#
