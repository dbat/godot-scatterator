#@tool
##@icon("res://addons/dabber/assets/icons/dabDab.small.svg")
#class_name dabPackedSceneExtractor
#extends Resource
#
### New dev as of April 2024
#
#const _rwMetadata = {
	#"display_class_name_as" : &"Extractor",
	#"category" : &"Dabber Nodes"
#}
#
##const VERBOSE := false
#
##@tool
##@icon("res://addons/dabber/assets/icons/dab3dResource.small.svg")
##class_name dab3DResource
##extends Resource
#
###----------------------------------------------#
##const _rwMetadata = {
	##"display_class_name_as" : &"Dab3D Resource",
	##"category" : &"Dabber Nodes"
##}
#
#
#const VERBOSE := false
#
##const NSdabber = preload("res://addons/dabber/src/ns.gd")
#
##----------------------------------------------#
#
### A name to help you recognize this dab3DResource
#@export var name:String
#
### Choose a directory where your mesh resources can be
### saved to.
#var _last_path:String
#@export_dir var extract_path:String:
	#set(p):
		#extract_path = p
		#_last_path = p
	#get:
		#if _last_path != extract_path:
			#pass
			## re-extract
			## then emit_changed()
		#return extract_path
#
#var _last_ps:PackedScene
### Which PackedScene to use
#@export var packed_scene:PackedScene:
	#set(ps):
		#packed_scene = ps
		#_last_ps = ps
	#get:
		#if _last_ps != packed_scene:
			## There was a change?
			## re-extract
			## emit_changed
			#pass
		#return packed_scene
#
#
#@export var mesh_sources : Array[Resource]:
	#set(arr):
		#mesh_sources = arr
		#_last_mesh_sources = mesh_sources.duplicate()
		#if VERBOSE: print("set arr:", arr)
	#get:
		#if VERBOSE:
			#print("---TOP--------")
			#print("  mesh_sources:", mesh_sources)
			#print("  TOP _last_mesh_sources:", _last_mesh_sources)
		#if _last_mesh_sources != mesh_sources:
			#if VERBOSE:
				#print("Arrays differ")
			## Think about it: what we catch here, when we know the
			## two arrays differ, is that ONE item  has been ADDED or RMd
			## Only ONE --> will be in 'mesh_source_to_re_extract'
			#mesh_source_to_re_extract = mesh_sources.filter(
					#func(r): return true if not r in _last_mesh_sources else false
			#).pop_back()
			#if VERBOSE:
				#print("      ---NE--------")
				#print("      NEWB:", mesh_source_to_re_extract)
#
			## nb: do this before the emit!
			#_last_mesh_sources = mesh_sources.duplicate()
#
			## Then tell upstream that I have changed.
			#emit_changed()
			#mesh_source_to_re_extract = null
		#return mesh_sources
#
#
##----------------------------------------------#
#var mesh_source_to_re_extract:Resource
#
#
##----------------------------------------------#
#var _last_mesh_sources:Array[Resource]
#
#
##----------------------------------------------#
#var _dabs_array_size:int
#var _cached_meshes:Dictionary
#
#
#static func _h1(s): print("=".repeat(8),s,"=".repeat(8))
#
#func give_me_your_meshes():
	#return extract()
#
#
#func extract(dab:NSdabber.dabDab)->Array[Mesh]:
#
#
	#const verbose = VERBOSE
#
	#var retmesh : Array[Mesh]
	#var panic := false
#
	#var meshes : Array[Mesh]
#
	##for res in dab.mesh_sources:
	#var res = packed_scene
#
	#if res.resource_path.is_empty():
		#push_warning("Can't use ", res, " because its path is bad.")
		#panic = true
#
	#var extracted_mesh_rp := ""
#
	#var err:int
#
	## Check if the dab mesh_source_to_re_extract is the current res
	#var force_re_extract:= dab.mesh_source_to_re_extract == res
#
	#if res is PackedScene or res is dabMeshSourceFilter:
		## There is a way to painfully peek into packed scenes
		## without having to instance them. Maybe in future.
		## For now, I will instance.
		#var S : Node
		#if res is PackedScene:
			#S = res.instantiate()
		##else: #it's a dabMeshSourceFilter
			##if not res.packed_scene: continue
			##S = res.packed_scene.instantiate()
#
		## Get first VisualInstance3D from the scene
		## NB It looks in the children - not the root or its siblings.
		#var vnodes : Array = S.find_children("*","VisualInstance3D", false)
		##print("vnodes find:", vnodes)
		#if vnodes.is_empty():
			#push_error("Can't find any nodes with meshes in ", S)
			#panic = true#continue #next res
#
		#var _arr_pref:Array
		#var _arr_avoid:Array
		#if res is dabMeshSourceFilter:
			#_arr_pref = res.prefer.map(
				#func(np):return np.get_name(np.get_name_count()-1))
			#_arr_avoid = res.avoid.map(
				#func(np):return np.get_name(np.get_name_count()-1))
#
		#for vnode in vnodes:
			## Who do we exclude/include?
			#var skip_vnode
			#if res is dabMeshSourceFilter:
				#var _inpref:bool = StringName(vnode.name) in _arr_pref
				#var _inavoid:bool = StringName(vnode.name) in _arr_avoid
				#skip_vnode = true
				#if _arr_pref.is_empty(): skip_vnode = false
				#if _inpref: skip_vnode = false
				#if _inavoid: skip_vnode = true
#
			#if verbose:
				#print("SKIP ", skip_vnode, " FOR:", vnode.name)
				#if skip_vnode:
					#print("======================")
					#print("    WE ARE SKIPPING IT")
					#print("======================")
#
			#if skip_vnode:
				#continue
#
			#if vnode is CSGCombiner3D:
				## Make mesh and save as own resource (if file not there)
#
				#var rp_file_name:String
				#if res is dabMeshSourceFilter:
					#rp_file_name = res.packed_scene.resource_path.get_file()
				#else: # packedscene or other
					#rp_file_name = res.resource_path.get_file()
#
				## Make the name of what the resource would be
				#extracted_mesh_rp = "%s/%s.%s.tres" % \
					#[dab.extract_path, rp_file_name, vnode.name]
				#if verbose:
					#_h1("CSG")
#
				#if not force_re_extract: # then we can seek the saved res file!
					#if FileAccess.file_exists(extracted_mesh_rp):
						## get that resource instance and return it! (or make it)
						#var old_mesh = ResourceLoader.load(extracted_mesh_rp)
						#if verbose:
							#print(" CSG resource file exists, returning old_mesh: %s" % old_mesh)
						#meshes.append(old_mesh)
						#continue #next node
#
				## FILE IS NOT THERE - or we are forcing a reextract - We must do work
				## wow, _update_shape, vital line! undocumented.
				## get_meshes fails sometimes without it.
				#vnode._update_shape()
				#var a = vnode.get_meshes() # get the mesh from the CSG
				#if verbose: print(" Extracting mesh")
				#if a.is_empty():
					#push_error("No meshes found in CSG")
					#continue #next node
#
				#var new_mesh : Mesh = a[1]
				#if verbose:
					#print("  Found mesh. Saving mesh to: %s" % extracted_mesh_rp)
#
				## For some reason, this change to new_mesh does
				## *NOT* cause changed signal to happen, at least
				## not out to rwnode. Neither do any of them in this file.
				## TODO: Grok why...
				#new_mesh.resource_name = vnode.name
#
				#new_mesh.take_over_path(extracted_mesh_rp)
				#err = ResourceSaver.save(new_mesh)
				#if err != OK:
					#if verbose: print("err:%s" % err)
					##15 ERR_FILE_UNRECOGNIZED == did not take_over before save
					#panic = true
					#break
				#if verbose: print("  Appending: %s" % new_mesh)
				#meshes.append(new_mesh)
#
			## vnode is a MeshInstance3D with a mesh (maybe)
			#elif vnode is MeshInstance3D:
				#if verbose:
					#_h1("MeshInstance3D")
				## Sanitize:
				#if not vnode.mesh:
					#push_error("The meshinstance's mesh is null")
					#continue #next node
#
				## grab the res path in case it's there
				#var vnode_mesh_rp = vnode.mesh.resource_path
#
				#var rp_file_name:String
				#if res is dabMeshSourceFilter:
					#rp_file_name = res.packed_scene.resource_path.get_file()
				#else: # packedscene or other
					#rp_file_name = res.resource_path.get_file()
#
				### because of how I split the filename later, I don't want
				### any dots other than in the extensions.
				##if rp_file_name.count(".",0) > 1:
					##push_error("The PackedScene name (",
						##rp_file_name,") should not have '.' characters in it")
					##continue #next node
#
				## path/PSceneName.MI3dNodeName.tres
				#extracted_mesh_rp = "%s/%s.%s.tres" % \
						#[dab.extract_path, rp_file_name, vnode.name]
#
				##todo
				#if not force_re_extract and \
					#FileAccess.file_exists(extracted_mesh_rp):
					## get that resource instance and return it! (or make it)
					#var old_mesh = ResourceLoader.load(extracted_mesh_rp)
					#if verbose:
						#print("Exists as a file, appending mesh: %s" % old_mesh)
					#meshes.append(old_mesh)
				#else:
					#if verbose: print("MeshInstance3D mesh is %s" % vnode.mesh)
#
					## look at the res path. if it has :: in it then we must
					## extract it and save it to a file.
					#var savestate := ""
					#if vnode_mesh_rp.contains("::"):
						#if verbose:
							#print(" Mesh has :: take_over_path: %s" % extracted_mesh_rp)
						#vnode.mesh.take_over_path(extracted_mesh_rp)#must do before save!
						#savestate="SAVE"
#
					## seek the saved resource file, in case it's been deleted
					#if not FileAccess.file_exists(extracted_mesh_rp):
						#savestate = "SAVE"
#
					#var new_mesh:Mesh
					#new_mesh = vnode.mesh
					#new_mesh.resource_name = vnode.name
					#new_mesh.resource_path = extracted_mesh_rp
#
					#if verbose:
						#print("  Assigning name: %s" % new_mesh.resource_name)
#
					#if savestate == "SAVE":
						#if verbose: print("  Saving the resource file.")
						#err = ResourceSaver.save(new_mesh)
						#if err != OK:
							##15 ERR_FILE_UNRECOGNIZED == do take_over before save
							##19 ERR_CANT_OPEN
							#if verbose: print("err:%s" % err)
							#if err == 19:
								#push_error("Check your dabDab.extract_path")
							#panic = true
							#break
					#if verbose: print("  Appending Mesh: %s" % new_mesh)
					#meshes.append(new_mesh)
			#else: # None of the mesh types above
				## No dice.
				#push_error("Cannot find a mesh to extract in:", vnode)
		## NEXT NODE in PackedScene
		#if panic:
			#break
#
	## NEXT RES in dab.mesh_sources
	#if panic:
		#return []
#
	#retmesh.append_array(meshes)
	#return retmesh
#
#
#
#
#
##
##
##func extract(dab:NSdabber.dabDab)->Array[Mesh]:
##
##
	##const verbose = VERBOSE
##
	##var retmesh : Array[Mesh]
	##var panic := false
##
	##var meshes : Array[Mesh]
##
	##for res in dab.mesh_sources:
##
		##if res.resource_path.is_empty():
			##push_warning("Can't use ", res, " because its path is bad.")
			##continue
##
		##var extracted_mesh_rp := ""
##
		##var err:int
##
		### Check if the dab mesh_source_to_re_extract is the current res
		##var force_re_extract:= dab.mesh_source_to_re_extract == res
##
		##if res is PackedScene or res is dabMeshSourceFilter:
			### There is a way to painfully peek into packed scenes
			### without having to instance them. Maybe in future.
			### For now, I will instance.
			##var S : Node
			##if res is PackedScene:
				##S = res.instantiate()
			##else: #it's a dabMeshSourceFilter
				##if not res.packed_scene: continue
				##S = res.packed_scene.instantiate()
##
			### Get first VisualInstance3D from the scene
			### NB It looks in the children - not the root or its siblings.
			##var vnodes : Array = S.find_children("*","VisualInstance3D", false)
			###print("vnodes find:", vnodes)
			##if vnodes.is_empty():
				##push_error("Can't find any nodes with meshes in ", S)
				##continue #next res
##
			##var _arr_pref:Array
			##var _arr_avoid:Array
			##if res is dabMeshSourceFilter:
				##_arr_pref = res.prefer.map(
					##func(np):return np.get_name(np.get_name_count()-1))
				##_arr_avoid = res.avoid.map(
					##func(np):return np.get_name(np.get_name_count()-1))
##
			##for vnode in vnodes:
				### Who do we exclude/include?
				##var skip_vnode
				##if res is dabMeshSourceFilter:
					##var _inpref:bool = StringName(vnode.name) in _arr_pref
					##var _inavoid:bool = StringName(vnode.name) in _arr_avoid
					##skip_vnode = true
					##if _arr_pref.is_empty(): skip_vnode = false
					##if _inpref: skip_vnode = false
					##if _inavoid: skip_vnode = true
##
				##if verbose:
					##print("SKIP ", skip_vnode, " FOR:", vnode.name)
					##if skip_vnode:
						##print("======================")
						##print("    WE ARE SKIPPING IT")
						##print("======================")
##
				##if skip_vnode:
					##continue
##
				##if vnode is CSGCombiner3D:
					### Make mesh and save as own resource (if file not there)
##
					##var rp_file_name:String
					##if res is dabMeshSourceFilter:
						##rp_file_name = res.packed_scene.resource_path.get_file()
					##else: # packedscene or other
						##rp_file_name = res.resource_path.get_file()
##
					### Make the name of what the resource would be
					##extracted_mesh_rp = "%s/%s.%s.tres" % \
						##[dab.extract_path, rp_file_name, vnode.name]
					##if verbose:
						##_h1("CSG")
##
					##if not force_re_extract: # then we can seek the saved res file!
						##if FileAccess.file_exists(extracted_mesh_rp):
							### get that resource instance and return it! (or make it)
							##var old_mesh = ResourceLoader.load(extracted_mesh_rp)
							##if verbose:
								##print(" CSG resource file exists, returning old_mesh: %s" % old_mesh)
							##meshes.append(old_mesh)
							##continue #next node
##
					### FILE IS NOT THERE - or we are forcing a reextract - We must do work
					### wow, _update_shape, vital line! undocumented.
					### get_meshes fails sometimes without it.
					##vnode._update_shape()
					##var a = vnode.get_meshes() # get the mesh from the CSG
					##if verbose: print(" Extracting mesh")
					##if a.is_empty():
						##push_error("No meshes found in CSG")
						##continue #next node
##
					##var new_mesh : Mesh = a[1]
					##if verbose:
						##print("  Found mesh. Saving mesh to: %s" % extracted_mesh_rp)
##
					### For some reason, this change to new_mesh does
					### *NOT* cause changed signal to happen, at least
					### not out to rwnode. Neither do any of them in this file.
					### TODO: Grok why...
					##new_mesh.resource_name = vnode.name
##
					##new_mesh.take_over_path(extracted_mesh_rp)
					##err = ResourceSaver.save(new_mesh)
					##if err != OK:
						##if verbose: print("err:%s" % err)
						###15 ERR_FILE_UNRECOGNIZED == did not take_over before save
						##panic = true
						##break
					##if verbose: print("  Appending: %s" % new_mesh)
					##meshes.append(new_mesh)
##
				### vnode is a MeshInstance3D with a mesh (maybe)
				##elif vnode is MeshInstance3D:
					##if verbose:
						##_h1("MeshInstance3D")
					### Sanitize:
					##if not vnode.mesh:
						##push_error("The meshinstance's mesh is null")
						##continue #next node
##
					### grab the res path in case it's there
					##var vnode_mesh_rp = vnode.mesh.resource_path
##
					##var rp_file_name:String
					##if res is dabMeshSourceFilter:
						##rp_file_name = res.packed_scene.resource_path.get_file()
					##else: # packedscene or other
						##rp_file_name = res.resource_path.get_file()
##
					#### because of how I split the filename later, I don't want
					#### any dots other than in the extensions.
					###if rp_file_name.count(".",0) > 1:
						###push_error("The PackedScene name (",
							###rp_file_name,") should not have '.' characters in it")
						###continue #next node
##
					### path/PSceneName.MI3dNodeName.tres
					##extracted_mesh_rp = "%s/%s.%s.tres" % \
							##[dab.extract_path, rp_file_name, vnode.name]
##
					###todo
					##if not force_re_extract and \
						##FileAccess.file_exists(extracted_mesh_rp):
						### get that resource instance and return it! (or make it)
						##var old_mesh = ResourceLoader.load(extracted_mesh_rp)
						##if verbose:
							##print("Exists as a file, appending mesh: %s" % old_mesh)
						##meshes.append(old_mesh)
					##else:
						##if verbose: print("MeshInstance3D mesh is %s" % vnode.mesh)
##
						### look at the res path. if it has :: in it then we must
						### extract it and save it to a file.
						##var savestate := ""
						##if vnode_mesh_rp.contains("::"):
							##if verbose:
								##print(" Mesh has :: take_over_path: %s" % extracted_mesh_rp)
							##vnode.mesh.take_over_path(extracted_mesh_rp)#must do before save!
							##savestate="SAVE"
##
						### seek the saved resource file, in case it's been deleted
						##if not FileAccess.file_exists(extracted_mesh_rp):
							##savestate = "SAVE"
##
						##var new_mesh:Mesh
						##new_mesh = vnode.mesh
						##new_mesh.resource_name = vnode.name
						##new_mesh.resource_path = extracted_mesh_rp
##
						##if verbose:
							##print("  Assigning name: %s" % new_mesh.resource_name)
##
						##if savestate == "SAVE":
							##if verbose: print("  Saving the resource file.")
							##err = ResourceSaver.save(new_mesh)
							##if err != OK:
								###15 ERR_FILE_UNRECOGNIZED == do take_over before save
								###19 ERR_CANT_OPEN
								##if verbose: print("err:%s" % err)
								##if err == 19:
									##push_error("Check your dabDab.extract_path")
								##panic = true
								##break
						##if verbose: print("  Appending Mesh: %s" % new_mesh)
						##meshes.append(new_mesh)
				##else: # None of the mesh types above
					### No dice.
					##push_error("Cannot find a mesh to extract in:", vnode)
			### NEXT NODE in PackedScene
			##if panic:
				##break
		##elif res is dabFolder:
			### TODO
			##meshes = res.get_meshes()
			##pass
		##elif res is Mesh:
			##if verbose:
				##_h1("Mesh Resource appended.")
			##meshes.append(res)
		##elif res is PrimitiveMesh:
			##if verbose:
				##_h1("PrimitiveMesh appended.")
			##meshes.append(res)
##
		##if panic:
			##break
	### NEXT RES in dab.mesh_sources
	##if panic:
		##return []
##
	##retmesh.append_array(meshes)
	##return retmesh
##
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
##region Maybe NB later
#
##static func _resource_path_ok(res)->bool:
##	if res.resource_path.is_empty():
##		push_error("The packed scene %s has no resource path. Weird." % res)
##		return false
##	return true
#
#
##func _ls(path)->Array:
##	var files = []
##	var dir = DirAccess.open(path)
##	if dir:
##		dir.list_dir_begin()
##		var file_name = dir.get_next()
##		while file_name != "":
##			if not dir.current_is_dir():
##				files.append(file_name)
##			file_name = dir.get_next()
##	else:
##		assert(false, "An error occurred when trying to access the path: %s" % path)
##	return files
##
##
##func _rm_resources(path:String)->int:
##	_instD.on(true)
##	var _D = _instD.D
##	# seek and del file - going to ignore most errors
##	# we need to del %s%s*.tres ...
##	# mushroom.tscn.tres (.scn)
##	# quaternius_pack.tscn.BirchTree_1.tres (.scn)
##	var psname_and_ext : String = path.get_file()
##	var rp_file_name : String = psname_and_ext.split(".")[0]
##
##	var files = _ls(extract_path)
##	files = files.filter(func(f): return f.begins_with(rp_file_name))
##	for file in files:
##		var extracted_mesh_rp = "%s%s" % \
##			[extract_path, file]
##		_D.say("removing : %s" % extracted_mesh_rp)
##		var err = DirAccess.remove_absolute(extracted_mesh_rp)
##		if err:# == ERR_FILE_BAD_DRIVE:
##			return err
##
##	return 0
## ------------------------------------
##		# get all the filenames
##		# filter for those with the filename (not entire path)
##		# and those are the ones to rm
##		var old_rp := old_res.resource_path
##		_D.say("NEW_DISCONNECTION for %s" % old_rp)
##		var files = _ls(extract_path)
##		print(old_rp.get_file())
##		#print(files)
##		files = files.filter(func(f): return f.begins_with(old_rp.get_file()))
##		# Trying to filter-out any resources that are not currently being used
##		# But, this is a problem because they just got instanced in the setter...
##		# The ref count climbs quickly. has_cached always returns true...
##		# what else to do?
##		# Going to just delete them all for now. Hoping that other users of the
##		# resources won't panic. I think the resources will just be made again
##		# whenever this dabMesh is set again.
###
###		files = files.filter(func(f):
###			var rp = "%s%s" % [extract_path, f.get_file()]
###			print("  rp:", rp, " has cached ", not ResourceLoader.has_cached(rp))
###			return true if not ResourceLoader.has_cached(rp) else false)
##		_D.h1("filtered")
##		#_D.say(files)
##		for file in files:
##			var rp = "%s%s" % [extract_path, file]
##			_D.say(" pretending to rm %s" % file)
##			var err = 0#_rm_resources(rp)
##			#var err = DirAccess.remove_absolute(old_rp)
##			if err == ERR_FILE_BAD_DRIVE:
##				state = STATE.DISK_ERROR
##				return []
#
#
##endregion
