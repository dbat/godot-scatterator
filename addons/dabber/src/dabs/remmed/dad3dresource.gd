#@tool
#@icon("res://addons/dabber/assets/icons/dab3dResource.small.svg")
#class_name dab3DResource
#extends Resource
#
##----------------------------------------------#
#const _rwMetadata = {
	#"display_class_name_as" : &"Dab3D Resource",
	#"category" : &"Dabber Nodes"
#}
#
#
#const VERBOSE := false
#
##const NSdabber = preload("res://addons/dabber/src/ns.gd")
#
##----------------------------------------------#
#
### A name to help you recognize this dab3DResource
#@export var name:String
#
#
### Add all the fans you wish to deploy.
#@export var dabs : Array[NSdabber.dabDab]:
	#set(arr): # ONLY FIRES WHEN ENTIRE ARRAY IS SET
		#dabs = arr
		#_dabs_array_size = dabs.size()
	#get:
		## get is a problem when the inspector is open
		## because it runs all the time.
		## I'd not have any code in here at all, but I
		## want to know when the dabs array has changed.
		## i.e a dab has been appended or removed
		## or the entire array is cleared.
		#if dabs.size() != _dabs_array_size:
			#_dabs_array_size = dabs.size()
			#_cache_meshes()
			#_connect_to_changed_signals()
		#return dabs
#
### If you use a mix texture, the dabs will be
### distributed across the texture by their index
### in the dabs array.
#@export var mix_texture : Texture2D
#
#
### How much to mingle the mix edges by
#@export_range(0, 100.0, 1, "or_greater") var mix_mingle : float = 1.0
#
##----------------------------------------------#
#var _dabs_array_size:int
#var _cached_meshes:Dictionary
#
#
#static func _h1(s): print("=".repeat(8),s,"=".repeat(8))
#
##----------------------------------------------#
#func cache_meshes():
	##print("cache_meshes:", self)
	##print(self.get_instance_id())
	##print()
	#if _cached_meshes.is_empty():
		#_cache_meshes()
#
#
#func get_meshes(dab:NSdabber.dabDab)->Array[Mesh]:
	#return _cached_meshes[dab]
#
##----------------------------------------------#
#
#func _connect_to_changed_signals():
	#const verbose = VERBOSE
	#for dab in dabs:
		#if verbose:
			#print("Connecting to signals dab.changed and dabNameFilter.changed")
		##TODO Keep a personal list of all connections so I can clean them?
		##Is this even a problem?
		#dab.changed.connect(_cache_meshes, CONNECT_REFERENCE_COUNTED)
		#for srcres:Resource in dab.mesh_sources:
			#if srcres is dabMeshSourceFilter:
				#srcres.changed.connect(_cache_meshes, CONNECT_REFERENCE_COUNTED)
#
#
#func _cache_meshes() -> void:
	#const verbose = VERBOSE
	#if verbose: print("There was a change in dab3DResource or a dabDab, so caching meshes.")
	#_cached_meshes.clear()
	#for dab in dabs:
		#var yep := DirAccess.dir_exists_absolute(dab.extract_path)
		#if yep:
			#var a := extract(dab)
			##_cached_meshes[dab] = a
			#_cached_meshes[dab] = dab.give_me_your_meshes()
		#else:
			#push_error("Error making a directory at %s" % dab.extract_path)
