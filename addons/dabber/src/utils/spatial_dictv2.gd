class_name SpatialDict
extends RefCounted

## In a nutshell:
## 1. Take a point (vec3) and turn it into a key that is relative to
##    a low-res voxel-like slicing of space so that it can refer to
##    an area around it
## 2. Any points inserted are then turned into keys, which immediately
##    makes them relative to that voxel space and that point is then
##    stored in a list within that dict entry
## 3. Looking up stuff is the same as 2 but just return the list at
##    the key.

var step_size : float
var snap : float
var snapped : Vector3
var spatial_dict : Dictionary
var list_of_refs:Array

func _init(_step_size:float = 10) -> void:
	step_size = _step_size
	snap = snapped((1.0/_step_size),0.01)
	snapped = Vector3(snap,snap,snap)

func del():
	spatial_dict.clear()
	list_of_refs.clear()

func is_empty()->bool:
	return spatial_dict.is_empty()

#func reset(cs:float = 100):
#	spatial_dict.clear()
#	self.step_size = cs

func _hash(point:Vector3)->Vector3:
	return Vector3(
		point.x,
		point.y,
		point.z,
	).snapped(snapped)



#func find_objects_at_point(point)->Array:
#	var foo = spatial_dict.get(_hash(point),[])
#	print("Find at:", _hash(point))
#	return foo

#func is_at_point(global_point, object)->bool:
#	var hgp:Vector3 = _hash(global_point)
#	print("is at?", hgp)
#	var foo = spatial_dict.get(hgp, [])
#	var b = object in foo
#	if b:
#		print(" Found it at:", hgp)
#	return b


## Does not care about what object may be there
## It's only about is there something there at all?
func anything_at_point(global_point)->bool:
	var hgp:Vector3 = _hash(global_point)
	#print("  Looking for:", hgp)
	var ret:=spatial_dict.has(hgp)
	#if ret: print("**** Something is there:", hgp)
	return ret


func insert_object_at_point(point, object=null):
	var key = _hash(point)
	#print("Insert key:", key)
	var index := 0
	if object:
		list_of_refs.append(object)
		index = list_of_refs.size()-1
	if spatial_dict.has(key):
		spatial_dict[key].append(index)
	else:
		spatial_dict[key] = [index]


func insert_object_at_aabb(global_aabb:AABB, object=null):
	var min := _hash(global_aabb.position)
	var max := _hash(global_aabb.end)

	var step_x:float = min.x
	var step_y:float = min.y
	var step_z:float = min.z
	while step_x < max.x:
		while step_y < max.y:
			while step_z < max.z:
				insert_object_at_point(Vector3(step_x, step_y, step_z))
				step_z += snap
			step_z = min.z
			step_y += snap
		step_y = min.y
		step_x += snap
	print("step_x:", step_x)
	print("step_y:", step_y)
	print("step_z:", step_z)
	print()
