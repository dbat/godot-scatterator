class_name dabUtils
extends Object

# MIT License
# Modified Work Copyright (c) 2023-present Donn Ingle.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

static func get_luminance(img:Image, where:Vector3, size:Vector3) -> float:
	const verbose := false
	var nh = img.get_height()
	var nw = img.get_width()
	#if img.get_format() == How the heck do I test for srgb space?
		#img to linear() because get_luminance requires linear space

	if verbose:
		print("where:", where)
		print("nw,nh: %s" % Vector2(nw, nh))

	var nx = remap(where.x, -(size.x/2.0),
			+(size.x/2.0),0,nw)
	var nz = remap(where.z, -(size.z/2.0),
			+(size.z/2.0),0,nh)

	if verbose: print("nx,nz: %s" % Vector2(nx, nz))

	# have to clamp this mofo
	nx = clampf(nx, 0, nw-1)
	nz = clampf(nz, 0, nh-1)
	if verbose: print("clamped nx,nz: %s" % Vector2(nx, nz))

	var rgb:Color = img.get_pixel(nx, nz)
	#rgb = rgb.srgb_to_linear()
	#if verbose: print("rgb: %s" % rgb)
	var _lum:float = rgb.get_luminance()
	_lum = smoothstep(0,1,_lum)
	return rgb.get_luminance()


static func get_luminance_from_top_row(img:Image, x:float, width:float) -> float:
	const verbose := false
	#if img.get_format() == How the heck do I test for srgb space?
		#img to linear() because get_luminance requires linear space

	var nw = img.get_width()

	if verbose:print("width:", width, " nw:", nw, " x:", x)

	var nx = remap(x,
		-(width/2.0), +(width/2.0),
		0.0, nw)

	if verbose:print("  nx remapped:", nx)

	# have to clamp this mofo
	nx = clampf(nx, 0, nw-1)
	if verbose: print("  nx,nz: %s" % Vector2(nx,0))

	var rgb:Color = img.get_pixel(nx, 0)
	if verbose: print("  rgb: %s" % rgb)
	var _lum:float = rgb.get_luminance()
	_lum = smoothstep(0,1,_lum)
	return rgb.get_luminance()


static func dict_set_from_get_or_new(_d, _class):
	_d[_class] = _d.get(_class, _class.new())


#static func _get_image_from_texture_if_in_dict(_settings, _c) -> Image:
	#var tex:Texture
	#var img:Image
	#if _c in _settings:
		#tex = _settings[_c].get(&"texture") # or null
		#if tex is Texture2D:
			#img = tex.get_image()
			#return img
	#return null

#static func get_image_or_null_from_named_resource_property(res:Resource, propname:StringName) -> Image:
	#var tex:Texture
	#var img:Image
	#var tex_in:Texture2D = res.get(propname)
	#if tex_in and tex_in is Texture2D:
		#img = tex_in.get_image()
		#return img
	#return null
