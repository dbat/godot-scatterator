@static_unload
@tool
class_name NSdabber
extends EditorPlugin

# MIT License
#
# Copyright (c) 2023 Donn Ingle (donn.ingle@gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

## Dabber Plugin
##
## TODO Document me


# Weird consts to get around Godot not coping with class_names in the gd files.

const dab3D := preload("src/dab3dnode/dab3d.gd")
const dabUtils = preload("src/utils/utils.gd")

# Gizmo
const CursorGizmos := preload("src/gizmos/gizmos_plugin.gd")

# UI
const preloaded_script_button_plugin := preload("src/ui/inspector/inspector_customize_plugin.gd")
const preloaded_quick_dab_button_scene := preload("src/ui/spatial_bar/quickbutton.tscn")

# Cursors
const CursorBase = preload("src/ui/cursors/cursor_base.gd")
static var cursor_dict_class_to_paf:Dictionary

# Dabs
const dab3DResource := preload("src/dabs/dad3dresource.gd")


# Settings & Algorithms
const dabSettingsBase = preload("src/dabs/settings/dab_settings_base.gd")
const dabSettingsMasking = preload("src/dabs/settings/dab_settings_masking.gd")
const dabSettingsRotation = preload("src/dabs/settings/dab_settings_rotation.gd")
const dabSettingsScale = preload("src/dabs/settings/dab_settings_scale.gd")

const dabMeshCustomDataBase = preload("src/dabs/dab_mesh_custom_data_base.gd")

const dabFolder = preload("src/dabs/dab_folder.gd")
#const dabPackedsceneExtractor = preload("src/dabs/dab_packedscene_extractor.gd")
const dabPathFilter = preload("src/dabs/dab_path_filter.gd")

const dabDab := preload("src/dabs/dab.gd")



# Assets
const PREF_AVOID_SCENE = preload("src/dabs/pref_avoid.tscn")
const cursor_material := preload("assets/cursor.tres" )
const DEFAULT_SPHERE := preload("assets/default_mesh.tres")
const NODE3D_ICON := preload("assets/icons/dab3DNode.small.svg")
#const DAB_3D_GIZMO_ICON := preload("assets/icons/dab3DNode.small.svg")

const bStyleBasic := preload("assets/button_basic_style.tres")
const bStyleMain := preload("assets/button_main_style.tres")

const InspectorToolButton := preload("src/ui/inspector/inspector_button.gd")
const InspectorIcon := preload("src/ui/inspector/inspector_icon.gd")
const InspectorRadio := preload("src/ui/inspector/inspector_radio.gd")


const _VERBOSE := false


var button_plugin
var quick_dab_button_scene
var quick_dab_button
var current_dab3dnode:NSdabber.dab3D
var has_the_scene_changed:bool

# Our custom gizmo that will work for/with the custom Node 3D
var gizmo_plugin = CursorGizmos.new()


# This is here and not in dabCursorBase because I could not get
# static vars to hold this list in that file. Search me.
static var cursors:String = "":
	get:
		var a := cursor_dict_class_to_paf.values().map(
			func(cls):
				# Cheeky little load to instance them to get their name
				var MyClass = load(cls)
				var inst = MyClass.new()
				return inst.name
		)
		return ",".join(a)


# Just the classnames of the cursors
static var cursors_classnames_array:Array:
	get:
		return cursor_dict_class_to_paf.keys()


func _enable_plugin() -> void:
	if not is_connected("scene_changed", _scene_changed):
		scene_changed.connect(_scene_changed)


func _scene_changed(s):
	const verbose = _VERBOSE
	if verbose: print("scene changed: %s" % s)
	#has_the_scene_changed = true
	current_dab3dnode = null
	quick_dab_button.visible = false


# "enter tree" makes little sense, but hey...
func _enter_tree() -> void:
	if Engine.is_editor_hint():
		#if not EditorInterface.is_plugin_enabled("res://addons/resource_wrangler/"):
		print("
======================= DABBER TIPS =============================
If you have just installed this plugin, restart your project.

SUGGESTION: Install the \"Resource Wrangler\" plugin to best
work with Dabber. Without it, you will have to manually configure
all sub-resources.
=================================================================
		")

	# Find all dabCursorX classes and their paths
	cursor_dict_class_to_paf = CursorBase.get_cursor_class_paths()#objects()

	add_custom_type(
		"dab3D", # must be the same as the class_name in dab3d.gd
		"Node3D",
		NSdabber.dab3D,
		create_main_icon()
		)

	#TODO STARTUP ERROR new on null. I remmed it out for now.
	#assert(NSdabber.dab3D.new() != null, "NSdabber.dab3D fails")

	# PS - had to move this before the 3d gizmo to get it to work again ?
	# Add our custom Go button!
	button_plugin = preloaded_script_button_plugin.new()
	add_inspector_plugin(button_plugin)

	# Here's our custom Gizmo
	add_node_3d_gizmo_plugin(gizmo_plugin)

	# Add an extra button for easier re-scattering
	quick_dab_button_scene = preloaded_quick_dab_button_scene
	quick_dab_button = quick_dab_button_scene.instantiate()
	quick_dab_button.quick_dab_signal.connect(_quickdab)
	add_control_to_container(EditorPlugin.CONTAINER_SPATIAL_EDITOR_MENU, quick_dab_button)


# happens on deactivating the plugin
# TODO: This is weird. When does the plugin (as a singular thing) ever leave
# the tree? deleting a dab3d node does not seem to fire this. Disabling the plugin
# does. But should this code not be under _disable_plugin() instead?
func _exit_tree() -> void:
	#print("I leave tree:", self.name)
	current_dab3dnode = null
	cursor_dict_class_to_paf.clear()

	remove_custom_type("NSdabber.dab3D")
	remove_node_3d_gizmo_plugin(gizmo_plugin)
	remove_inspector_plugin(button_plugin)
	remove_control_from_container(EditorPlugin.CONTAINER_SPATIAL_EDITOR_MENU, quick_dab_button)

	#NB : Without doing this, godot segfaults.
	quick_dab_button.queue_free()


# Runs on any click anywhere in editor.
# Also runs on a load of the scene.
# This means I can use this to tell when focus has changed!
func _handles(object: Object):
	const verbose = _VERBOSE
	if verbose: print("\nplugin.gd _handles(%s)" % object)#.resource_path)

	var last_known_dab3d := current_dab3dnode # For ease of reading the logic

	var last_known_was_dab3d := (
		last_known_dab3d != null                # if it exists ↓
		and is_instance_valid(last_known_dab3d) # and it's okay ↓
		and last_known_dab3d is NSdabber.dab3D  # and it's a dab3D
	)
	if verbose: print("last_known_was_dab3d:", last_known_was_dab3d)

	var this_object_is_dab3d := ( # the incoming 'object' arg
		object != null						# if it exists ↓
		and is_instance_valid(object) # and it's okay ↓
		and object is NSdabber.dab3D  # and it's a dab3d
	)
	if verbose: print("this_object_is_dab3d:", this_object_is_dab3d)

	var dab3d_node_changed := (
		this_object_is_dab3d 			 # if arg is a dab3d
		and last_known_was_dab3d		 # and last one was too
		and object != last_known_dab3d # and they are different
	)
	if verbose: print("dabs changed:", dab3d_node_changed)

	var state : StringName

	if not last_known_was_dab3d:
		if this_object_is_dab3d:
			state = &"new_button"
		else:
			state = &"no_button"

	if last_known_was_dab3d:
		if not this_object_is_dab3d:
			state = &"keep_last_button"
		if dab3d_node_changed:
			state = &"new_button"

	if verbose: print("state %s" % state)

	# case on a new install of plugin when quick_dab_button did not yet exist!
	if quick_dab_button: # thus the if.
		match state:
			&"new_button":
				if last_known_was_dab3d: last_known_dab3d.my_gizmo_hide()
				current_dab3dnode = object
				current_dab3dnode.my_gizmo_show()
				quick_dab_button.text = "Dab %s" % object.name
				quick_dab_button.visible = true
			&"no_button":
				current_dab3dnode = null
				quick_dab_button.visible = false
				return false
			&"keep_last_button":
				# The idea here is that you may select something else
				# and still want to run a dab on the last seleted dab3d node
				# hence the button remains for that. So you don't have to
				# keep switching back to the node all the time.
				pass

	return true


func _make_visible(visible: bool):
	if quick_dab_button:
		quick_dab_button.visible = visible


func _quickdab():
	const verbose = _VERBOSE
	if verbose: print("current_dab3dnode: %s" % current_dab3dnode)
	if current_dab3dnode:
		current_dab3dnode.rescatter_if_in_editor()


func _get_plugin_name() -> String:
	return "dabber"


func _get_plugin_icon() -> Texture2D:
	return create_main_icon()


func create_main_icon(scale: float = 1.0) -> Texture2D:
	var size: Vector2 = Vector2(16, 16) * get_editor_interface().get_editor_scale() * scale
	var NODE3D_ICON = NSdabber.NODE3D_ICON as Texture2D
	var image: Image = NODE3D_ICON.get_image()
	image.resize(size.x, size.y, Image.INTERPOLATE_TRILINEAR)
	return ImageTexture.create_from_image(image)
