Dabber (early version)
==

HOWTO
=
Fetch the code. Also go to my Resource Wrangler plugin and fetch that too. https://gitlab.com/dbat/godot-resource-wrangler

1. Copy the addons/dabber directory into your own project addons.
1. Copy the addons/resource_wrangler directory into your own project addons.
1. Enable the plugins in settings.
1. Open the dabber/docs/demo tree and look around.
1. Open Resource Wrangler from the icon at the bottom.
1. In the demo scene, look for the dab3D nodes and look at the Inspector to get going.

Please send me feedback on my email: donn.ingle@gmail.com
Or here on Gitlab.
Or on Mastodon. https://mastodon.gamedev.place/deck/@dbat


NOTES
=
Groups must go on the Static Bodies - to influence rays. (not the mesh and not the collider)
