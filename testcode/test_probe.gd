@tool
extends MeshInstance3D

@export var go:bool:
	set(b):
		go = false
		#print(bar)
		foo()

#@onready var bar = $"../floor"


#func _physics_process(delta: float) -> void:
	#foo()

class sphere:
	var cen:Vector3
	var rad:float
	var mmidx:int
	var xleft:float:
		get: return cen.x - rad
	var xright:float:
		get: return cen.x + rad
	var ytop:float:
		get: return cen.y - rad
	var ybot:float:
		get: return cen.y + rad
	var zfront:float:
		get: return cen.z - rad
	var zback:float:
		get: return cen.z + rad

	func _init(aabb,idx=-1):
		cen = aabb.get_center()
		rad = (aabb.position - cen).length()
		mmidx = idx

	func _print():
		print("sphere:", mmidx)
		print("  cen:", cen)
		print("  rad:", rad)
		print()

func foo():
	print("===================\n")

	var X:Array
	var Y:Array
	var Z:Array

	var mi:MultiMesh = $"../test".multimesh
	var oaabb:AABB = mi.mesh.get_aabb()

	for index in mi.instance_count:
		var t = mi.get_instance_transform(index)
		var aabb:AABB = t * oaabb
		var sp := sphere.new(aabb, index)
		X.append(sp)
		sp._print()
		#X.append([index, aabb.position.x])
		#Y.append([index, aabb.position.y])
		#Z.append([index, aabb.position.z])

	var candidates:Array

	var paabb:AABB = transform * self.get_aabb()
	var psp = sphere.new(paabb)
	print("psp")
	psp._print()

	X.sort_custom(func(sp1, sp2): return sp1.cen.x < sp2.cen.x )

	print("sorted:")
	for sp in X:
		print(sp)
		sp._print()
	print()
	print("Seek probe x:", psp.cen.x)

	var index = X.bsearch_custom(psp,
		func(sp:sphere, probe:sphere):
				#print(probe.mmidx)
				#print(sp.mmidx)
				#var left_edge_probe = probe.cen.x - probe.rad
				#var left_edge_sp = sp.cen.x - sp.rad
				#print("    probe vs:", sp.mmidx)
				#print("      left_edge_probe:", left_edge_probe)
				#print("      < left_edge_sp?:", left_edge_sp)
				#and must return true if the first argument is less than the second, and return false otherwise.
				return sp.xleft < probe.xleft
				#return left_edge_sp < left_edge_probe
	)

	print("Evryone (and this index) is to the rhs of me:", index)
	for i in range(index,X.size()):
		print("RHS of me:")
		X[i]._print()
	print()

	index = X.bsearch_custom(psp,
		func(sp:sphere, probe:sphere):
				#print(probe.mmidx)
				#print(sp.mmidx)
				#var right_edge_probe = probe.cen.x + probe.rad
				#var right_edge_sp = sp.cen.x + sp.rad
				#print("    probe vs:", sp.mmidx)
				#print("      right_edge_probe:", right_edge_probe)
				#print("      < right_edge_sp?:", right_edge_sp)
				#and must return true if the first argument is less than the second, and return false otherwise.
				#return right_edge_sp < right_edge_probe
				return sp.xright < probe.xright
	)

	print("And this index down to index 0 is lhs of me:", index)
	for i in range(0,index+1):
		print("LHS of me:")
		X[i]._print()
	print()


	return



#func good_foo():
	#print("===================\n")
#
	#var X:Array
	#var Y:Array
	#var Z:Array
#
	#var mi:MultiMesh = $"../test".multimesh
	#var oaabb:AABB = mi.mesh.get_aabb()
#
	#for index in mi.instance_count:
		#var t = mi.get_instance_transform(index)
		#var aabb:AABB = t * oaabb
		#var sp := sphere.new()
		#sp.cen = aabb.get_center()
		#sp.rad = (aabb.position - sp.cen).length()
		#sp.mmidx = index
		#X.append(sp)
		#sp._print()
		##X.append([index, aabb.position.x])
		##Y.append([index, aabb.position.y])
		##Z.append([index, aabb.position.z])
#
	#var candidates:Array
	#var paabb:AABB = transform * self.get_aabb()
	#var psp = sphere.new()
	#psp.cen = paabb.get_center()
	#psp.rad = (paabb.position - psp.cen).length()
	#print("psp")
	#psp._print()
#
	#X.sort_custom(func(sp1, sp2):
		## return sp1a[1] > b[1])
		#return sp1.cen.x < sp2.cen.x
		#)
	#print("sorted:")
	#for sp in X:
		#print(sp)
		#sp._print()
	#print()
	#print("Seek probe x:", psp.cen.x)
#
	##var cmp = func(sp, probe):
		###print(probe.mmidx)
		###print(sp.mmidx)
		##var left_edge_probe = probe.cen.x - probe.rad
		##var left_edge_sp = sp.cen.x - sp.rad
		##print("    probe vs:", sp.mmidx)
		##print("      left_edge_probe:", left_edge_probe)
		##print("      < left_edge_sp?:", left_edge_sp)
		###and must return true if the first argument is less than the second, and return false otherwise.
		##return left_edge_sp < left_edge_probe
#
	#var index = X.bsearch_custom(psp,
		#func(sp, probe):
				##print(probe.mmidx)
				##print(sp.mmidx)
				#var left_edge_probe = probe.cen.x - probe.rad
				#var left_edge_sp = sp.cen.x - sp.rad
				#print("    probe vs:", sp.mmidx)
				#print("      left_edge_probe:", left_edge_probe)
				#print("      < left_edge_sp?:", left_edge_sp)
				##and must return true if the first argument is less than the second, and return false otherwise.
				#return left_edge_sp < left_edge_probe
	#)
#
	#print("Evryone (and this index) is to the rhs of me:", index)
	#for i in range(index,X.size()):
		#print("RHS of me:")
		#X[i]._print()
	#print()
#
	#index = X.bsearch_custom(psp,
		#func(sp, probe):
				##print(probe.mmidx)
				##print(sp.mmidx)
				#var right_edge_probe = probe.cen.x + probe.rad
				#var right_edge_sp = sp.cen.x + sp.rad
				#print("    probe vs:", sp.mmidx)
				#print("      right_edge_probe:", right_edge_probe)
				#print("      < right_edge_sp?:", right_edge_sp)
				##and must return true if the first argument is less than the second, and return false otherwise.
				#return right_edge_sp < right_edge_probe
	#)
#
	#print("And this index down to index 0 is lhs of me:", index)
	#for i in range(0,index+1):
		#print("LHS of me:")
		#X[i]._print()
	#print()
#
#
	#return






func foox():

	var X:Array
	var Y:Array
	var Z:Array

	var mi:MultiMesh = $"../test".multimesh
	var oaabb:AABB = mi.mesh.get_aabb()
	for index in mi.instance_count:
		var t = mi.get_instance_transform(index)
		var aabb:AABB = t * oaabb
		X.append([index, aabb.position.x])
		Y.append([index, aabb.position.y])
		Z.append([index, aabb.position.z])

	var candidates:Array
	var paabb:AABB = transform * self.get_aabb()

	X.sort_custom(func(a, b): return a[1] > b[1])
	for i in X: print(i)
	print("Seek:", paabb.position.x)
	var cmp = func(a,v): print("a:",a); print("v:", v);return a[1]>=v
	var index = X.bsearch_custom(paabb.position.x, cmp, true )
	print(index)
	print()
	return

	# seek
	#int bsearch_custom(value: Variant, func: Callable, before: bool = true) const
	#
	#Finds the index of an existing value (or the insertion index that maintains sorting order, if the value is not yet present in the array) using binary search and a custom comparison method. Optionally, a before specifier can be passed. If false, the returned index comes after all existing entries of the value in the array. The custom method receives two arguments (an element from the array and the value searched for) and must return true if the first argument is less than the second, and return false otherwise.
	#
	#Note: Calling bsearch_custom() on an unsorted array results in unexpected behavior.

	#var index = X.bsearch_custom(paabb.position.x, func(a,b): return a[1]>=b[1])
	for Le in X:
		if paabb.position.x >= Le[1]:
			# get the RHS of this one
			var idx = Le[0]
			var rhs = (oaabb * mi.get_instance_transform(idx)).end.x
			if paabb.end.x <= rhs:
				candidates.append(Le)
	if not candidates:
		return
	# possible ret with no hit


	Y.sort_custom(func(a, b): return a[1] > b[1])
	# seek
	# possible ret with no hit
	Z.sort_custom(func(a, b): return a[1] > b[1])














## Fails :(
##
## Sadly can't detect mmeshinstances

#func foo():
	#var space_state = get_world_3d().direct_space_state
	#var params:=PhysicsShapeQueryParameters3D.new()
	#params.shape = capshape
	#params.transform = %probe.transform
	#params.collide_with_areas = true
	#params.collide_with_bodies = true
	#params.collision_mask = 1
	#var hits := space_state.intersect_shape(params,1)
	#print(hits)
	#if hits:
		#for h in hits:
			#print(h)
#
#
#func dedup(array: Array) -> Array:
	#var unique: Array = []
	#for item in array:
		#if not unique.has(item):
			#unique.append(item)
	#return unique
