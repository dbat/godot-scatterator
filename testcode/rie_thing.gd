@tool
extends Node
## Nested enum example
##
## Quickly hacked together example for how one could expose a different "sub" enum variable
## depending on the value of a "main" enum.


# Main enum
enum MainClass {
	WARRIOR,
	MAGE,
	ROGUE,
}

# Sub enums
enum WarriorClass {
	TANK,
	MONK,
}

enum MageClass {
	GLASS_CANNON,
	HEALER,
}

enum RogueClass {
	RANGER,
	THIEF,
}

# Regular export variable for selecting main class
@export var main_class: MainClass:
	set(value):
		main_class = value
		# Needed so the sub class gets updated when changing main class
		notify_property_list_changed()

# Internal variables to store sub class index
var _warrior_class: WarriorClass
var _mage_class: MageClass
var _rogue_class: RogueClass

# Helper function
func get_subclasses(p_main_class: MainClass) -> Dictionary:
	match p_main_class:
		MainClass.WARRIOR: 	return WarriorClass
		MainClass.MAGE: 	return MageClass
		MainClass.ROGUE: 	return RogueClass
	return {}


func _get(property: StringName) -> Variant:
	if property == &"sub_class":
		print("get ran")
		match main_class:
			MainClass.WARRIOR: 	return _warrior_class
			MainClass.MAGE: 	return _mage_class
			MainClass.ROGUE: 	return _rogue_class
	return null


func _set(property: StringName, value: Variant) -> bool:
	if property == &"sub_class":
		match main_class:
			MainClass.WARRIOR: 	_warrior_class =	value
			MainClass.MAGE: 	_mage_class = 		value
			MainClass.ROGUE: 	_rogue_class = 		value
		return true
	return false


func _get_property_list() -> Array[Dictionary]:
	var sub_class_property := {
		"name": "sub_class",
		"type": TYPE_INT,
		"hint": PROPERTY_HINT_ENUM,
		# Convert enum entries into readable strings and concatonate into a comma-separated list
		"hint_string": ", ".join(get_subclasses(main_class).keys()).replace("_", " ").to_pascal_case(),
		# Save the property
		"usage": PROPERTY_USAGE_DEFAULT,
	}
	return [sub_class_property]
