@tool
extends Node2D

func _notification(what: int) -> void:
	var p = $"../.."
	var sig:Signal = p.bigsig
	#if not sig.is_connected(_blargh):
#		sig.connect(_blargh)
	sig.connect(_blargh, CONNECT_REFERENCE_COUNTED)
	
func _blargh():
	print("%s got bigsig" % name)
