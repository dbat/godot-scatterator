@tool
extends Node3D

var A:RID
var state:PhysicsDirectSpaceState3D

@export var tog:bool:
	set(b):
		tog = false
		create()
		go()

func create():
	state = owner.get_world_3d().direct_space_state

	PhysicsServer3D.free_rid(A)

	A = PhysicsServer3D.area_create()
	PhysicsServer3D.area_set_space(A, state)
	PhysicsServer3D.area_set_transform(A, self.transform)
	var areas_transform = PhysicsServer3D.area_get_transform(A)
	print("area's transform:", areas_transform)
	print("mine            :", self.transform)

	#mystery command. No clue. It feels like add_child, but who knows?
	#PhysicsServer3D.area_attach_object_instance_id(A,self.get_instance_id())

	#Seems to make no difference
	PhysicsServer3D.area_set_monitorable(A, true)

	# Also makes no diff
	#PhysicsServer3D.area_set_ray_pickable(A, true)

func go():
	await probe()

func probe():
	print()
	print("*****************************")

	# Loop through those mesh instances and create shapes
	# and test for collision with the area

	PhysicsServer3D.area_clear_shapes(A)

	# for each meshinstance as b
	for b:MeshInstance3D in get_children():
		# make a shape
		var shape_rid = PhysicsServer3D.box_shape_create()
		PhysicsServer3D.shape_set_data(shape_rid, b.mesh.size)

		#As per docs
		var params:PhysicsShapeQueryParameters3D = PhysicsShapeQueryParameters3D.new()
		params.shape_rid = shape_rid
		params.transform = b.transform
		params.collide_with_bodies = false
		params.collide_with_areas = true

		# Execute physics queries here...
		var surrounds = state.intersect_shape(params,16)

		# Debug output
		print()
		print("Made shape on:", b.name)
		print(" orig pos, size:", b.position, " ", b.mesh.size)
		print(" shape pos, size:",
				params.transform.origin,
				PhysicsServer3D.shape_get_data(shape_rid)
		)
		print(str(surrounds).replace("}, {", "\n"))

		# If we did not hit anything, place a shape into the area
		# for the next round
		if surrounds.is_empty():
			var newt:Transform3D = params.transform
			PhysicsServer3D.area_add_shape(A, shape_rid, newt)
			print(" set as:", newt)

			await get_tree().process_frame

	print()
	print("Number of shapes:", PhysicsServer3D.area_get_shape_count(A))
	for s in range(0,PhysicsServer3D.area_get_shape_count(A)):
		var st = PhysicsServer3D.area_get_shape_transform(A,s)
		print("  :", st)
