extends Node3D


func _ready() -> void:
	var MM := MultiMesh.new()
	
	var PL := load("res://tests/donut.tscn")
	var mesh := ResourceLoader.load("res://tests/donut.tscn::TorusMesh_a1mey")
	print("mesh:", mesh)
	MM.mesh = mesh
	
#	var ball := SphereMesh.new()
#	ball.radius = 1
#	ball.height = 2
#	MM.mesh = ball
	
	MM.transform_format = MultiMesh.TRANSFORM_3D
	MM.instance_count = 1
	
	var t := Transform3D(global_transform)
	MM.set_instance_transform(0,t)
	
	$MultiMeshInstance3D.multimesh = MM
