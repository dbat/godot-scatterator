@tool
extends Node2D

@export var tog:bool:
	set(b):
		test()

@export var arr:Array[String]:
	set(v):
		print("v is:", v)
		arr = v
	get:
		#print("get:", arr)
		return arr

func test():
	arr.clear()
	arr = ["a","b","c"] # v is:["a", "b", "c"]
	arr[0] = "bar" # nothing
	arr.append("foo") # nothing

	print("fetch arr:", arr)

