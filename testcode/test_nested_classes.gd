@tool
extends Node2D

@export var tog:bool:
	set(b):
		tog = false
		test()

func test():
	A.B.run()
	A.B.C.run()
	Foo.get_names()
	
class A:
	class B:
		static func run(): print("A.B.run runs")
		class C:
			static func run(): print("A.B.C.run runs")

class Foo:
	static func get_names():
		var all = Foo.Inner.new().get_method_list()
		all = all.filter(func(e): return e.id == 0 and e.flags==1 and e.name != "free")
		print(str(all).replace(", \"", ",\n  \""))
		#print( all.filter(func(e): return 
	class Inner:
		static func innera()->Vector2:return Vector2.ZERO
		static func innerb():pass
