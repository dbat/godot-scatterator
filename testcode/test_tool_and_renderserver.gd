@tool
extends Node3D

@export var make:bool:
	set(t):
		make = false
		#if not mat:
			#mat = StandardMaterial3D.new()
		mk_hitbox_by_server()

@export var del:bool:
	set(t):
		del = false
		for r in rid:
			RenderingServer.free_rid(r)
		rid.clear()

var rid:Array[RID]
var bmeshes:Array[BoxMesh]
var mat:StandardMaterial3D

func mk_hitbox_by_server():
	# Create a visual instance (for 3D).
	var instance_rid = RenderingServer.instance_create()
	rid.append(instance_rid)
	# Set the scenario from the world, this ensures it
	# appears with the same objects as the scene.
	var scenario = get_world_3d().scenario

	RenderingServer.instance_set_scenario(instance_rid, scenario)
	var bmesh:BoxMesh = BoxMesh.new()
	bmeshes.append(bmesh)
	bmesh.size = Vector3(0.5,0.5,0.5)#.ONE#aabb.size
	bmesh.material = StandardMaterial3D.new()#mat
	RenderingServer.instance_set_base(instance_rid, bmesh.get_rid())
	# Move the mesh around.
	var xform = Transform3D(Basis(), Vector3(randf()*2,randf()*2,randf()*2))#aabb.get_center())
	RenderingServer.instance_set_transform(instance_rid, xform)
	RenderingServer.instance_set_visible(instance_rid, true)
	#RenderingServer.instance_set_ignore_culling(instance_rid, true)
	print(instance_rid)
