@tool
extends Node2D

#var R = preload("res://addons/dabber/resource_scripts/scattering/rays/again/blergv3.gd")

@export var tog:bool:
	set(b):
		tog = false
		test()
		
func test():
	for c in dabRays.CursorEnum:
		print("---------%s-------------" % c)
		var cursor=dabRays.CursorEnum[c]
		var directions_csv = dabRays.get_directions_csv(cursor)
		print("all the directions_csv:", directions_csv)
		for direction in dabRays.get_directions(cursor):
			#var direction = dabRays.Dir.UP
			var rays = dabRays.get_rays(cursor, direction)
			var rays_csv = dabRays.get_rays_csv(rays)
			#print("  dir:", direction)
			print("    rays csv:", rays_csv)
			print("    rays:", rays)
	print()
	print()
