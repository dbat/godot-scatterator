@tool
extends Node2D

@export var tog:bool:
	set(b):
		test()

@export var Averylongnamesoicanseeit:float


var _last_path:String
@export_dir var path:String:
	set(p):
		path = p
		_last_path = p
		print("SET")
	get:
		return path
		# No point doing this for Strings, at least!
		#if _last_path != path:
		#	print("GET after a change")

func test():
	global_scale = Vector2(10,10)
	print(global_scale)
