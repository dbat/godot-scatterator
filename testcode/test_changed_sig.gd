@tool
extends Node2D

@export var test:bool:
	set(b):
		test = false
		go()

class CUSTRES extends Resource:
	@export var myvar:int

var resA:CUSTRES = CUSTRES.new()

func go():
	#if resA.is_connected("changed",foo):
	resA.disconnect("changed",foo)
	resA.connect("changed", foo)
	print()
	resA.myvar = 22 # no changed is emitted


	var _sigs = resA.get_signal_connection_list("changed")
#[{ "signal": Resource::[signal]changed, "callable": Node2D(test_changed_sig.gd)::foo, "flags": 0 }]
	resA.disconnect("changed",foo)
	resA.resource_name="trigger?" # changed is emitted
	resA.connect("changed", foo)

func foo():
	print("changed ran...")

